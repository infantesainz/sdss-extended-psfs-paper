# Generate plot of all PSF images
#
# This Python script makes the plot of the radial profiles for all PSFs
# obtained in this work. It also plots the radial profiles from de Jong 2008
# for SDSS g, r and i bands. To be able to compare the profiles, first of
# all they are normalized by the sume of all pixels. Taking into account
# that the radial profiles from de Jong 2008 have different pixel step, it
# is necessary to interpolate them and having the same pixel grid.
#
# Original author:
#     Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s):
# Copyright (C) 2019, Raul Infante-Sainz.
#
# This Python script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Python script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.






# Import modules
import sys
import numpy as np
from astropy.io import fits

# Matplotlib crashes in Mac OS systems
# To avoid this crash, use the condition:
from sys import platform as sys_pf
if sys_pf == 'darwin':
    import matplotlib
    matplotlib.use("TkAgg")

import matplotlib.pyplot as plt
from matplotlib.patches import Circle
import matplotlib.gridspec as gridspec
from matplotlib.colors import LogNorm, SymLogNorm
from mpl_toolkits.axes_grid1 import ImageGrid, make_axes_locatable
plt.style.use('classic')




# Read input arguments
psfs_profile_aux       = sys.argv[1]    # All PSF profiles
psfs_profile_auxdejong = sys.argv[2]    # All PSF profiles of de Jong
output_plot            = sys.argv[-1]   # Target, name of the final plot

# First argument is all PSF images separated by a space, it is necessary to
# construct a list from this string. To do that, use `.split()'.
psfims = psfs_profile_aux.split()
psfims_jong = psfs_profile_auxdejong.split()


a = 0.75
c = 2000
zp = 22.5
pixelscale = 0.396

f_arcmin_to_pix = 60.0/0.396
ticks_arcmin = np.array([-9, -7, -5, -3, -1, 0, 1, 3, 5, 7, 9])
ticks_labels = np.array(['9', '7', '5', '3', '1', '0', '1', '3', '5', '7', '9'])
ticks_labels_n = np.array(['', '', '', '', '', '', '', '', '', '', ''])

ticks_pixels = ticks_arcmin * f_arcmin_to_pix
rtot = int(ticks_pixels[-1])

ticks_pixels = ticks_pixels + rtot


plt.clf()
fig = plt.figure()
#plt.grid()
ax1 = fig.add_subplot(111)
ax2 = ax1.twiny()



# Set colors and filter labels
colors  = ['tab:blue', 'tab:green', 'tab:red', 'tab:orange', 'tab:purple']
filters = ['u',        'g',         'r',       'i',          'z']

linestyles = ['dotted', 'dashdot',    'dashed',  'solid', (0, (1, 1))]

markers = ['P',        '*',         '^',       '.',          'v']
nmarkers = [None,      25-6,          25,        24+6,          None]

#markers = ['None',        'None',         'None',       'None',    'None']
msizes =  5*[5]


Nmarkers_jong = 20
Nmarkers_me = 1e3

n = 0
njong = 0
l_jong = []
l_yo = []
dynamicalrange = []
for filt, psfim in zip(filters, psfims):
    print(' ')
    print('FILTER', filt)

    # Get the maximun x value for profiles of de Jong
    jong = np.genfromtxt(psfims_jong[1])
    jong_xpix = jong[:, 0].astype(np.float32)
    xmax_jong = int(np.nanmax(jong_xpix))

    # de Jong 2008 profiles are only available for g, r, i bands
    if (filt == 'g') or (filt == 'r') or (filt == 'i'):

        """1D PSF PROFILES JONG"""
        a=0.9
        jong = np.genfromtxt(psfims_jong[njong])
        jong_xpix = jong[:, 0].astype(np.float32)
        xmax_jong = int(np.nanmax(jong_xpix))

        jong_y = jong[:, 1].astype(np.float32)

        # Because of de Jong profiles are not in steps of 1 pixel, I
        # interpolate the profiles to have the flux at steps of 1 pixels
        jong_y_interp = np.interp(np.arange(xmax_jong), jong_xpix, jong_y)

        jong_y_norm = jong_y / np.nansum(jong_y_interp)

        y_mag_jong = -2.5*np.log10(jong_y_norm)

        y_mag_jong_interp = -2.5*np.log10(jong_y_interp)


        # Interpolate to have few points
        xi_jong = jong_xpix*pixelscale
        yi_jong = y_mag_jong

        xi_jong_min = 0.01 #np.nanmin(xi_jong)
        xi_jong_max = np.nanmax(xi_jong)

        xn_jong = np.geomspace(xi_jong_min, xi_jong_max, nmarkers[n])
        yn_jong = np.interp(xn_jong, xi_jong, yi_jong)

        l1, = ax1.plot(xn_jong, yn_jong, color=colors[n], linestyle="None", \
                       alpha=a, markerfacecolor=None, markeredgecolor=colors[n], \
                       marker=markers[n], markersize=msizes[n], \
                       label=filt+'  de Jong (2008)')
        l_jong.append(l1)
        njong = njong + 1


    # Now plot the profiles of this work
    if True:
        """1D PSF PROFILES RAUL"""
        a = 0.8

        # Read radial profiles previously obtained
        alldata = fits.getdata(psfim, 1)
        xpix = alldata['radii_pixel'].astype(np.float32)
        x = xpix*pixelscale
        y = alldata['smean'].astype(np.float32)

        yori = y.copy()
        y_norm = y / np.nansum(y[:xmax_jong])
        y_mag = -2.5*np.log10(y_norm)

        dymag = np.nanmax(y_norm) - np.nanmin(y_norm)
        dx = np.nanmax(xpix) - np.nanmin(xpix)

        # Interpolate to have few points
        xi_me = xpix*pixelscale
        yi_me = y_mag

        xi_me_min = 0.01
        xi_me_max = np.nanmax(xi_me[:-3])
        xn_me = np.geomspace(xi_me_min, xi_me_max, Nmarkers_me)
        yn_me = np.interp(xn_me, xi_me, yi_me)

        l2, = ax1.plot(xn_me, yn_me, color=colors[n], linewidth=1.4, \
                       linestyle=linestyles[n], alpha=a, \
                       label=filt+'  This work')

        l_yo.append(l2)
        # Print on screen the dynamical flux range in magnitudes of the PSFs
        # obtained throught the radial profiles
        dyrange = np.nanmax(y_mag) - np.nanmin(y_mag)


        # Compute the slope considering a power law function:
        # y = a*x**-s
        # The slope is: s = log(y1/y2) / log(x1/x2)
        if True:
            # Indexes of two points where the slope is computed
            i1 = 5         # Corresponds to 5 * 0.396 = 2 arcsec
            i2 = 1060      # corresponds to 1060 * 0.396 / 60 = 7 arcmin

            # Compute the slope
            ys_min = y_norm[i2]
            ys_max = y_norm[i1]

            xs_min = xpix[i1]
            xs_max = xpix[i2]

            slope = (np.log(ys_max/ys_min))/(np.log(xs_min/xs_max))

        print('RANGE', filt, dyrange, slope)
        dynamicalrange.append([filt, dyrange, slope])

    n = n + 1


# Save data of radial profiles
dynamicalrange = np.array(dynamicalrange)
statistics_outfile = output_plot.replace('.pdf','.dat')
np.savetxt(statistics_outfile, dynamicalrange, fmt="%s", \
           header="FILTER, DYNAMICRANGE, PROFILESLOPE")

ax1.set_xlim(0.1, 540)
ax1.set_ylim(0, 22)
ax1.invert_yaxis()

ax1.set_ylabel(r'Surface brightness [mag/arcsec$^{2}$]')#, fontsize=16)

ax1.set_xlabel('Distance from centre [arcsec]')#,fontsize=16)
ax1.set_xscale('log')

ax1ticks_arcmin = [0.1, 1, 10, 100, 480]
ax1tlabels_arcmin = [0.1, 1, 10, 100, 480]

ax1.set_xticks(ax1ticks_arcmin)
ax1.set_xticklabels(ax1tlabels_arcmin)

first_legend = plt.legend(handles=l_jong, loc=3)#, fontsize=12)

ax = plt.gca().add_artist(first_legend)

ax1xlim = ax1.get_xlim()

ax2.set_xlim(np.log10(ax1xlim)) # Put limits in log10 of arcsecs
ax2ticks_arcmin = np.log10([  6,    30,    60,    120,    240,    480])
ax2tlabels_arcmin =        [0.1,    0.5,    1,     2,        4,      8]

ax2.set_xticks(ax2ticks_arcmin)
ax2.set_xticklabels(ax2tlabels_arcmin)
ax2.set_xlabel('Distance from centre [arcmin]')#, fontsize=16)
#ax2.grid(True,linestyle='--')

plt.legend(handles=l_yo, loc=1)#, fontsize=12)

#ax1.grid(True,linestyle=':')
#ax1.annotate('i-band bump', xy=(20,11), xytext=(35,9), \
#             arrowprops=dict(facecolor='black', shrink=0.01))

plt.savefig(output_plot, format='pdf', bbox_inches='tight')
print(output_plot, 'SAVED!')
