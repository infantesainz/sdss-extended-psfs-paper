# Create stamps for outer part of the PSF: iteration 2
#
# This script is the first iteration for building the outer part of the SDSS
# PSF. It downloads SDSS from an input catalogue that has been previously
# prepared, and then it performs some steps:
# - Recenter each image where the bright star is
# - Mask all objects except the bright star
# - Compute the radial profile and normalize in a given radius
# - Save the normalized and masked image, and also the statistics.
#
# Original author:
#     Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s):
# Copyright (C) 2019, Raul Infante-Sainz.
#
# This Python script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Python script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.


# Import mmodules
import os
import sys
import yaml
import argparse
import numpy as np
from astropy import wcs
from astropy.io import fits
import raulisfpy.rfastro as rfa
import raulisfpy.rfgeneral as rfg
from astropy.table import Table,Column





# Reading parameters from command line
if True:

    parser = argparse.ArgumentParser(description='Reading arguments from command line')

    parser.add_argument('--band',
        help='SDSS filter band (u g r i z)')

    parser.add_argument('--nmax', type=int,
        help='Max number of images')

    parser.add_argument('--starcat',
        help='Star catalogue')

    parser.add_argument('--yaml_cf',
        help='Configuration YAML file')

    parser.add_argument('--noisechisel_cf',
        help='NoiseChisel configuration file')

    parser.add_argument('--segment_cf',
        help='Segment configuration file')

    parser.add_argument('--output_file',
        help='Output file data')

    parser.add_argument('--path_out',
        help='Out path where all output files will be saved')

    parser.add_argument('--psf1',
        help='PSF from iteration 1')

    parser.add_argument('--tilesizesfile',
        help='File containing random tilesizes for NoiseChisel')

    args = parser.parse_args()


    # Define input files and parameters
    nmax                 = args.nmax
    band                 = args.band
    psf_it1              = args.psf1
    path_out             = args.path_out
    configfile           = args.yaml_cf
    output_file          = args.output_file
    tilesizesfile        = args.tilesizesfile
    stars_catalogue      = args.starcat
    segment_cfg_file     = args.segment_cf
    noisechisel_cfg_file = args.noisechisel_cf


    print('++++++++++++++++++++++++++++++')
    print('++++++++++++++++++++++++++++++')
    print('                              ')
    print('INPUT ARGUMENTS:', args        )
    print('                              ')
    print('++++++++++++++++++++++++++++++')
    print('++++++++++++++++++++++++++++++')


    # Read configuration file
    with open(configfile, 'r') as cfg_file:

        """ GENERAL PARAMETERS """
        cfg = yaml.load(cfg_file)
        pg  = cfg['GENERAL']

        dimpix      = pg['dimpix']
        pixelscale  = pg['pixelscale']
        rnormarcsec = pg['rnormarcsec']
        rprofile_max = pg['rprofile_max']

        """ SPECIFIC PARAMETERS"""
        pl          = cfg[band]
        mag_kw      = pl['mag_kw']
        reject      = pl['bleedmasking']['reject']

        y1_mask     = pl['bleedmasking']['y1_mask']
        y2_mask     = pl['bleedmasking']['y2_mask']

        x1_left     = pl['bleedmasking']['x1_left']
        x2_left     = pl['bleedmasking']['x2_left']

        x1_center   = pl['bleedmasking']['x1_center']
        x2_center   = pl['bleedmasking']['x2_center']

        x1_right    = pl['bleedmasking']['x1_right']
        x2_right    = pl['bleedmasking']['x2_right']


# Define the radius of normalization in pixels
rnormpix = int(rnormarcsec / pixelscale)

# Read table data with information from the fitst iteration
inputtable = Table(fits.getdata(stars_catalogue, 1))

# Read the obtained PSF data from iteration 1
dat_psf = fits.getdata(psf_it1, 1)

# Read random tilesizes already saved
tilesizes = np.genfromtxt(tilesizesfile, dtype=int)

rmlist = []
l_vnorm2 = []
n_toremove = []
l_logvnorm2 = []
l_im_normalized2 = []

n = 0
for image in inputtable:
    """ MAIN LOOP"""

    print(' ')
    print('#######################################')
    print(n, 'of', len(inputtable))
    print('#######################################')
    print(' ')

    # Define image to be none just in case there were some failure
    im_detected = 'None'
    image_masked1 = 'None'
    image_masked2 = 'None'
    im_normalized = 'None'
    im_noisechisel = 'None'

    # Read image names
    im_swarp = image['im_swarp']
    im_normalized1 = image['im_normalized1']

    # Extract information from the image
    hdu_in = fits.open(im_swarp)
    dat_in = hdu_in[0].data
    mag = image['mag']
    ra = image['ra']
    de = image['dec']
    vnorm = image['vnorm1']

    # Substract the PSF obtained in the first iteration
    dat_sub = dat_in - dat_psf*vnorm

    # Save the subtracted PSF image
    hdu_out = fits.PrimaryHDU(dat_sub.astype(np.float32))
    hdu_out.header = hdu_in[0].header
    hdu_out.header['HISTORY'] = 'PSF SUBTRACTED'
    im_psfsub = im_swarp + '_psfsub.fits'
    hdu_out.writeto(im_psfsub, overwrite=True)
    rmlist.append(im_psfsub)


    # Obtaining detection map with `astnoisechisel'
    print('')
    print('NOISECHISEL')
    print('')
    im_detected = im_psfsub + '_detected.fits'
    im_segmented = im_detected + '_segmented.fits'

    try:
        # Read the tilesize according to the star number
        tilesize = str(tilesizes[n, 0]) + ',' + str(tilesizes[n, 1])
        qthresh = '0.5'
        line_noisechisel = ('astnoisechisel --hdu=0 ' + im_psfsub +
                            ' --output=' + im_detected +
                            ' --tilesize=' + tilesize +
                            ' --interpnumngb=1 ' +
                            ' --qthresh=' + qthresh +
                            ' --minnumfalse=1' +
                            ' --config=' + noisechisel_cfg_file)
        os.system(line_noisechisel)

    except:
        # If noisechisel has failed, then remove the images and continue
        # with the following star.
        rmline = 'rm ' + im_psfsub + ' ' + im_detected
        os.system(rmline)
        n_toremove.append(n)
        n = n + 1
        continue

    # Obtaining object masks with `astsegment'
    print('')
    print('ASTSEGMENT')
    print('')
    try:
        # Segment parameters
        line_segment = ('astsegment ' + im_detected +
                        ' --output=' + im_segmented +
                        ' --tilesize=' + tilesize +
                        ' --interpnumngb=1 ' +
                        ' --minnumfalse=1' +
                        ' --config=' + segment_cfg_file)
        os.system(line_segment)

    except:
        # If segment has failed, then remove the images and continue
        # with the following star.
        rmline = 'rm ' + im_psfsub + ' ' + im_segmented + ' ' + im_detected
        os.system(rmline)
        n_toremove.append(n)
        n = n + 1
        continue

    # One more check, if any of the two images have been created, then
    # continue with the following star
    if os.path.isfile(im_segmented) == False or os.path.isfile(im_detected) == False:
        rmline = 'rm ' + im_psfsub + ' ' + im_segmented + ' ' + im_detected
        os.system(rmline)
        n_toremove.append(n)
        n = n + 1
        continue


    # Mask objects using segmentation map
    print('')
    print('MASKING')
    print('')

    image_masked1 = im_psfsub + '_mobjects.fits'

    objects = fits.getdata(im_segmented, 'OBJECTS')
    objects[objects != 0] = 1


    # Make a copy of the original input image
    dat_masked = dat_in.copy()

    # Mask the input image copy using segmentation objects map
    dat_masked[objects == 1] = np.nan

    # Obtain the radial profile
    profile = rfa.radial_profile(dat_masked, pixelscale, binsize=5, \
              function='sigma_clip', rboundaries=(0, rprofile_max), \
              sigma_cut=3.0, center=None, symetrize=None, save_profile=None)

    profile = profile['radial_profile']

    tprofile = Table(profile)
    hdu_prof = fits.BinTableHDU(tprofile)

    # Normalization value is obtained from the radial profile at a given
    # radius
    vnorm2 = profile['smedian'][rnormpix]

    # Check if the normalization value is nan, if so, remove images and
    # continue with the following star.
    if np.isnan(vnorm2):
        rmline = 'rm ' + im_detected + ' ' + im_psfsub + \
                 ' ' + image_masked1 + ' ' + im_segmented
        os.system(rmline)
        n_toremove.append(n)
        n = n + 1
        continue

    # If the normalization value is not nan, then normalize the masked data
    # by this value. Then save this as data to be combined in next steps.
    dat_norm = dat_masked / vnorm2
    hdu_star = fits.PrimaryHDU(dat_norm.astype(np.float32))

    # Read and write necessary data from the input to the output
    im_normalized2 = im_swarp + f'_{mag:.2f}_normalized2.fits'
    hdu_star.header = hdu_in[0].header
    hdu_star.header['VNORM2'] = vnorm2

    hdu_star.name = 'IMAGE'
    hdu_prof.name = 'PROFILE'

    hdu_star.verify('fix')
    hdu_prof.verify('fix')

    # Define a list with two hdu: one for the image data and the other for
    # the radial profile table. Then save it.
    new_hdul = fits.HDUList([hdu_star, hdu_prof])
    new_hdul.writeto(im_normalized2, overwrite=True)

    # Append values computed in this second iteration to the columns
    l_im_normalized2.append(im_normalized2)
    l_vnorm2.append(vnorm2)
    l_logvnorm2.append(np.log10(vnorm2))

    # Remove temporal and unnecessary data
    if True:
        os.system('rm ' + im_psfsub)
        os.system('rm ' + im_noisechisel)
        os.system('rm ' + image_masked1)
        os.system('rm ' + im_detected)
        os.system('rm ' + im_segmented)

    # Increment counter to continue with the following star
    n = n + 1


# Create columns from the list data
c_im_normalized2 = Column(l_im_normalized2, name='im_normalized2')
c_vnorm2 = Column(l_vnorm2, name='vnorm2')
c_logvnorm2 = Column(l_logvnorm2, name='logvnorm2')

# Make a copy of the input data table, then append the data generated in
# this script and save them into a new fits table.
outputtable = inputtable.copy()
outputtable.remove_rows(n_toremove)
outputtable.add_column(c_im_normalized2)
outputtable.add_column(c_vnorm2)
outputtable.add_column(c_logvnorm2)

outputtable.write(output_file, format='fits', overwrite=True)
