# Introduce metadata in final PSF images
#
# This Makefile is written to do inject metadata in the final PSF images.
# Metadta consists in contact information and other parameters to take into
# account how they have been built
#
#
# Original author:
#     Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s):
# Copyright (C) 2019, Raul Infante-Sainz.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# Directories
# -----------
#
final-outdir = $(BDIR)/final-psf-images





# Writting meta-data
# ------------------
#
final-psfs = $(foreach b,$(BANDS),$(final-outdir)/psf-filter-$(b).fits)
$(final-outdir): ; mkdir $@
$(final-psfs): $(final-outdir)/psf-filter-%.fits: \
               $(prepared-outdir)/psf-filter-%-cropped.fits \
               $(bouter-outdir)/stats-iteration-3-filter-%.fits \
               $(binter-outdir)/stats-iteration-3-filter-%.fits \
               $(binner-outdir)/stats-iteration-3-filter-%.fits \
               | $(final-outdir)

	# Copy the prerequisite as target, it will be the final PSF
	cp $< $@

	# Delete header keywords from crop step
	astfits $@ -h1 \
		--delete=ICF1_1 \
		--delete=ICF1_2

	# Read the joining radius from the joined images
	roi=$$(astfits $(joined-outdir)/psf-filter-$*.fits -h0 \
                       | grep AJ \
                       | awk '{print $$3}')
	rii=$$(astfits $(joined-outdir)/psf-filter-$*.fits -h0 \
                       | grep CJ \
                       | awk '{print $$3}')

	# Get the number of images used in each part
	nouter=$$(asttable $(word 2,$^) | wc -l)
	ninter=$$(asttable $(word 3,$^) | wc -l)
	ninner=$$(asttable $(word 4,$^) | wc -l)

	# Get the current version of the PSF
	version=$$(git describe --tags)

	# Write all information into the final PSF
	astfits $@ -h1 \
	  --write=/,"Relevant information about this PSF" \
	  --write=BAND,$*,"SDSS filter/band (u g r i z)" \
	  --write=SCALE,0.396,"SDSS images pixel scale (arcsec/pixel)" \
	  --write=ROUTER,$$roi,"Radius for joining outer and inter part (pixel)" \
	  --write=RINTER,$$rii,"Radius for joining inter and inner part (pixel)" \
	  --write=NOUTER,$$nouter,"Number of images stacked for the outer part" \
	  --write=NINTER,$$ninter,"Number of images stacked for the inter part" \
	  --write=NINNER,$$ninner,"Number of images stacked for the inner part" \
	  --write=EMAIL1,"infantesainz(at)gmail(dot)com","Contact email" \
	  --write=EMAIL2,"rinfante(at)iac(dot)es","Contact email" \
	  --write=VERSION,$$version,"PSF version number" \
	  --write=CITE,"Infante-Sainz et al. (2019)","Reference" \
	  --comment "Drift scanning direction is from left to right" \
	  --comment "Normalization is done to have SUM of all pixels equal to (roughly) 1.0" \
	  --comment "Please, cite \"Infante-Sainz et al. (2019)\" if you find this data useful" \
	  --comment "MNRAS, Vol 491, Issue 4, February 2020, Pages 5317-5329" \
	  --comment "DOI: https://doi.org/10.1093/mnras/stz3111"





# Copy the PSF images including version number in the name
# --------------------------------------------------------
#
# In order to be able to have different version of the PSFs generated, they
# are copied into a final directory with the version number of the actual
# commit of the project into the final name of the PSFs.
nversion = $(shell git describe --tags)
cv-outdir = $(BDIR)/versioned-psf-images
psfs-cv = $(foreach b,$(BANDS),$(cv-outdir)/psf-filter-$(b)-$(nversion).fits)
$(cv-outdir): ; mkdir $@
$(psfs-cv): $(cv-outdir)/psf-filter-%-$(nversion).fits: $(final-outdir)/psf-filter-%.fits \
	                                                | $(cv-outdir)
	# Copy the prerequisite into the target, they are the semame but
	# with the version number in the name of the target.
	cp $< $@





# Final TeX macro
# ---------------
#
# Make an empty .tex file as final file when all PSFs have been bad rows
# interpolated, rotated and cropped.
$(mtexdir)/write-metadata.tex: $(final-psfs) $(psfs-cv) | $(mtexdir)

	# Get the current version of the PSF
	hash=$$(git describe --tags)

	# Select only the version number without the commit hash
	nohash=$$(echo $$hash | sed -e"s/-/ /" | awk '{print $$1}')

	# Write the command
	echo "\newcommand{\projectversionnohash}{$$nohash}" > $@

