# Build the inner part of the PSF
#
# This Makefile is written to construct the inner part of the PSF. It takes
# a catalogue of very bright stars previously constructed by matching very
# faint stars and SDSS central position of images. As a consequence, this
# catalogue contains all necessary data of the faint (and in princple not
# saturated) stars and SDSS images in which they are, and the shell command
# to download the image.
#
# Iteration 1 consists in going star by star doing the following:
#   - Create a new image with the center in the center of the star.
#   - Obtain object detection map and mask the image.
#   - Compute the radial profile and normalize the image at given radius.
#   - Once these steps have been done for all stars, save a table with
#   global statistics.
#   -Finally, combine all masked and normalized images to obtain the PSF.
#
# Iteration 3 consists compare all single profies with the obtained profile
# of the PSF in the iteration 2. There are some radial profiles that differ
# in shape because they are not stars but galaxies. This fact is used to
# reject those objects from the final combination of stars to build the
# inner part.
#
# Finally, the images prepared following all the previous steps are combined
# to obtain the outer part of the PSF.
#
# This Makefile is written to construct the inner part of the PSF
#
# Original author:
#     Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s):
# Copyright (C) 2019, Raul Infante-Sainz.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# Directories
# -----------
#
binter-indir = $(BDIR)
binner-outdir = $(BDIR)/inner-part





# Build the inner part of the PSF: iteration 1
# --------------------------------------------
#
# First iteration of the building of the PSF consists in download the stars
# and prepare them to be stacked. The preparation consists in recenter each
# star into a new image. The script reads RA and DEC from USNO-B1 catalogue.
# Then it creates a catalogue from which the center position of the star is
# obained with better precission. To do that, `astnoisechisel',
# `astsegment' and `astmkcatalog' is used. Then, `swarp' uses this new
# center to resample and center the star. Once it has been done, objects are
# masked and the image is normalized to a given radius. The result are a lot
# of images ready to be combined an also a .fits table with all statistics
# for the following steps.
stars-catalogue-inner = $(INDIR)/stars-catalogue-inner.fits
pyscript-it1-inner = $(pythondir)/build-inner-stamps-iteration-1.py
stat-it1-files-inner = $(foreach b,$(BANDS),$(binner-outdir)/stats-iteration-1-filter-$(b).fits)
$(binner-outdir): ; mkdir $@
$(stat-it1-files-inner): $(binner-outdir)/stats-iteration-1-filter-%.fits: \
                         $(pyconfigdir)/inner-parameters.yaml \
                         | $(binner-outdir)
	python $(pyscript-it1-inner) \
	       --band=$* \
	       --nmax=$(NSTARS) \
	       --output_file=$@ \
	       --path_out=$(binner-outdir) \
	       --swarp_cf=$(swarp-cfgfile) \
	       --segment_cf=$(astsegment-cfgfile) \
	       --starcat=$(stars-catalogue-inner) \
	       --mkcatalog_cf=$(astmkcatalog-cfgfile) \
	       --noisechisel_cf=$(astnoisechisel-cfgfile) \
	       --yaml_cf=$(pyconfigdir)/inner-parameters.yaml






# Stack stamps: iteration 1
# -------------------------
#
# Stack all images saved in the stat files. They are already centered,
# masked and normalized.
psfs-it1-inner = $(foreach b,$(BANDS),$(binner-outdir)/psf-iteration-1-filter-$(b).fits)
$(psfs-it1-inner): $(binner-outdir)/psf-%.fits: $(binner-outdir)/stats-%.fits
	# Stack all images together
	astarithmetic $$(asttable $< -cim_normalized1) -g0 \
		      $$(echo $$(asttable $< -c1) | wc -w) \
	              $(STACKING-OPERATOR) --output=$@





# Generate radial profiles for each inner PSF
# -------------------------------------------
#
# Radial profiles are generated for each PSF. Each radial profile is saved
# in the same directory. To do the radial profiles, a Python script is used.
pyscript-radial-profile = $(pythondir)/make-radial-profile.py
radial-profile-psfs-inner = $(foreach b,$(BANDS),$(binner-outdir)/psf-iteration-1-filter-$(b)-profile.fits)
$(radial-profile-psfs-inner): $(binner-outdir)/psf-iteration-1-filter-%-profile.fits: \
	                $(binner-outdir)/psf-iteration-1-filter-%.fits
	# Compute the radial profile
	python $(pyscript-radial-profile) $< 1 $@





# Checking objects that are non stars: save table with ALL data
# --------------------------------------------------------------
#
# Rejecting non star objects for the final stacking combination
pyscript-it3-inner = $(pythondir)/build-inner-stamps-iteration-3.py
stat-it3-files-inner-all = $(foreach b,$(BANDS),$(binner-outdir)/stats-iteration-3-filter-$(b)-all.fits)
$(stat-it3-files-inner-all): $(binner-outdir)/stats-iteration-3-filter-%-all.fits: \
                             $(binner-outdir)/psf-iteration-1-filter-%-profile.fits \
                             $(pyconfigdir)/inner-parameters.yaml
	python $(pyscript-it3-inner) \
	       --band=$* \
	       --output_file=$@ \
	       --yaml_cf=$(pyconfigdir)/inner-parameters.yaml \
	       --starcat=$(binner-outdir)/stats-iteration-1-filter-$*.fits \
	       --psf1_profile=$(binner-outdir)/psf-iteration-1-filter-$*-profile.fits





# Filtering objects non stars objects: build table with only STARS, iteration 3
# -----------------------------------------------------------------------------
#
# From the input table with all data, select only those objects that have
# value of the `s' parameter below the smaxgood one.
stats-it3-files-inner = $(foreach b,$(BANDS),$(binner-outdir)/stats-iteration-3-filter-$(b).fits)
$(stats-it3-files-inner): $(binner-outdir)/stats-iteration-3-filter-%.fits: \
	                 $(binner-outdir)/stats-iteration-3-filter-%-all.fits
	# Stack all images together
	# Obtain maximun value of s to be good
	smaxgood=$$(aststatistics $< -csmaxgood --mean)
	asttable $< -cim_normalized1,s --range=s,0:$$smaxgood -o$@





# Stack stamps: iteration 3
# -------------------------
#
# Stack all images saved in the stat files. They are already centered,
# masked and normalized.
psfs-it3-inner = $(foreach b,$(BANDS),$(binner-outdir)/psf-iteration-3-filter-$(b).fits)
$(psfs-it3-inner): $(binner-outdir)/psf-iteration-3-filter-%.fits: \
	           $(binner-outdir)/stats-iteration-3-filter-%.fits
	# Stack all images together
	astarithmetic $$(asttable $< -cim_normalized1) -g0 \
		      $$(echo $$(asttable $< -c1) | wc -w) \
	              $(STACKING-OPERATOR) --output=$@





# Final TeX macro
# ---------------
#
# Make an empty .tex file as final file when all light images
# bias, dark and gain corrected have been obtained
#
$(mtexdir)/build-inner.tex: $(stat-it3-files-inner-all) $(psfs-it3-inner) | $(mtexdir)
	echo "%empty" > $@

