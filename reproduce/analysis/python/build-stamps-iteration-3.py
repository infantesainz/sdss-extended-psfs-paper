# Filter galaxies from the set of objects for inner part of the PSF
#
# This script is the third iteration for building the inner part of the SDSS
# PSF. It compares the radial profile of each individual object with the
# stacked radial profile of the PSF obtained in the iteration 2. It computes
# the value of the standard deviation in the `check region' defined around
# the normalization radius and perform sigma clipping of these values.
# Objects with high value of `s' means that the radial profile differs a lot
# of the stacked radial profile PSF. They will be rejected in the final
# stacking process.
#
# Original author:
#     Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s):
# Copyright (C) 2019, Raul Infante-Sainz.
#
# This Python script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Python script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# Import mmodules
import os
import sys
import yaml
import argparse
import numpy as np
from scipy import stats
from astropy.io import fits
import raulisfpy.rfastro as rfa
import raulisfpy.rfgeneral as rfg
from astropy.stats import sigma_clip
from astropy.table import Table,Column

# Matplotlib crashes in Mac OS systems
# To avoid this crash, use the condition:
from sys import platform as sys_pf
if sys_pf == 'darwin':
    import matplotlib
    matplotlib.use("TkAgg")

import matplotlib.pyplot as plt
#plt.ioff()




# Reading parameters from command line
if True:



    parser = argparse.ArgumentParser(description='Reading arguments from command line')

    parser.add_argument('--band',
        help='SDSS filter band (u g r i z)')

    parser.add_argument('--starcat',
        help='Star catalogue')

    parser.add_argument('--yaml_cf',
        help='Configuration YAML file')

    parser.add_argument('--output_file',
        help='Output file data')

    parser.add_argument('--plot_file',
        help='Plot showing the fitting')

    parser.add_argument('--table_all',
        help='Table with all output information')

    parser.add_argument('--table_filtered',
        help='Table with only good objects for final combination')

    args = parser.parse_args()


    # Define input files and parameters
    band                       = args.band
    plot_name                  = args.plot_file
    configfile                 = args.yaml_cf
    output_file                = args.output_file
    table_name_input           = args.starcat
    table_name_output_all      = args.table_all
    table_name_output_filtered = args.table_filtered

    print('++++++++++++++++++++++++++++++')
    print('++++++++++++++++++++++++++++++')
    print('                              ')
    print('INPUT ARGUMENTS:', args        )
    print('                              ')
    print('++++++++++++++++++++++++++++++')
    print('++++++++++++++++++++++++++++++')


with open(configfile) as cfg_file:
    """ INPUT PARAMETERS """

    cfg = yaml.load(cfg_file)
    pg = cfg['GENERAL']

    zp         = pg['zp']
    mlim       = pg['mlim']
    pdeg       = pg['pdeg']
    fvsky      = pg['fvsky']
    sclip      = pg['sreject']
    dimpix     = pg['dimpix']
    pixelscale = pg['pixelscale']    # arcsec/pixel


def pol(x, coefs):
    yval = np.zeros(x.shape)
    for n, coef in reversed(list(enumerate(reversed(coefs)))):
        yval = yval + coef*x**n
    return yval #coefs[0]*x**2 + coefs[1]*x #+ coefs[2]



vsky = 10**((-1.0/2.5)*(mlim-zp-5.0*np.log10(pixelscale)))


data = fits.getdata(table_name_input, 1)    # hdul[1].data
t_all = Table(data)


ims = data['im_normalized2']

ddata = []
l_rlim = []
for row in data:    # im in ims:
    im = row['im_normalized2']
    #vnorm2 = row['vnorm2']
    #logvnorm2 = row['logvnorm2']

    profile = fits.getdata(im, 'PROFILE')
    #vnorm2 = fits.getheader(im, 'IMAGE')['VNORM2']
    y_nonan = profile['smedian']
    y_nonan[np.isnan(y_nonan)] = 99

    rsky = rfa.find_nearest(y_nonan, vsky, axis=None)

    rlim = profile['radii_scale'][rsky['indexs'][0]]
    l_rlim.append(rlim)
    #logvnorm2 = np.log10(vnorm2)

    #ddata.append([logvnorm2, rlim, mlim, vsky])

c_rlim = Column(l_rlim, name='rlim')
c_vsky = Column(len(data)*[vsky], name='vsky')
t_all.add_column(c_rlim)
t_all.add_column(c_vsky)


r1 = np.log10(t_all['rlim'])
logvnorm2 = t_all['logvnorm2']
ims = t_all['im_normalized2']




## FILTERING iteration 1
mask = (~np.isnan(logvnorm2))

#fit1 = np.polyfit(logvnorm2[mask], r1[mask], pdeg)
fit1_data = stats.linregress(logvnorm2, r1)
fit1 = [fit1_data[0], fit1_data[1]]

fited1 = np.polyval(fit1, logvnorm2)
dif1 = fited1 - r1

#fdata1 = sigma_clip(dif1, sigma=sclip)
fdata1 = sigma_clip(dif1, sigma_lower=3, sigma_upper=3)

## FILTERING iteration 2
mag2 = logvnorm2[~fdata1.mask]
r2 = r1[~fdata1.mask]

#fit2 = np.polyfit(mag2, r2, pdeg)
fit2_data = stats.linregress(mag2, r2)
fit2 = [fit2_data[0], fit2_data[1]]
fited2 = np.polyval(fit2, logvnorm2)
dif2 = fited2 - r1

dif2 = fited2 - r1

#fdata2 = sigma_clip(dif2, sigma=sclip)
fdata2 = sigma_clip(dif2, sigma_lower=3, sigma_upper=7)

mag3 = logvnorm2[~fdata2.mask]
r3 = r1[~fdata2.mask]
ims_good = ims[~fdata2.mask]


## GOOD
plt.plot(mag3, r3, '.c', label='Good stars', alpha=1)


## REJECTED
xx_rejected = logvnorm2[fdata2.mask]
yy_rejected = r1[fdata2.mask]
plt.plot(xx_rejected, yy_rejected, 'xr', label='Rejected stars', alpha=1)    # All original data



## FIT
ncutx = 18 # Number of dots to not be plotted in xx direction
xx = logvnorm2
ixx = np.argsort(xx)
xx_sorted = xx[ixx[:-ncutx]]

yy = pol(logvnorm2, fit2)
yy_sorted = yy[ixx[:-ncutx]]

plt.plot(xx_sorted, yy_sorted, '-k', linewidth=3, label='Linear fit')

#plt.xlim(-1.2, 1.2)
#plt.ylim(2.35, 2.8)
fsize = 16
plt.xlabel(r'log$_{10}$(normalization) [nanomaggies]', fontsize = fsize)
plt.ylabel(r'log$_{10}$(R$_{mlim}$) [arcsec]', fontsize = fsize)
plt.ylim(2.25,2.85)
#plt.grid()
leg = plt.legend(frameon=True,loc=4)
leg.get_frame().set_edgecolor('k')

plt.savefig(plot_name, bbox_inches='tight')
print(plot_name, 'SAVED!'),
###############################
###############################

logr1_mask = pol(fvsky*logvnorm2, fit2)

r1_mask = 10**logr1_mask/pixelscale

r1_mask_col = Column(r1_mask, name='r1_mask')
t_all.add_column(r1_mask_col)  # Add vsky_counts column to the input table

r2_mask_col = Column(np.array(len(r1_mask)*[dimpix]), name='r2_mask')
t_all.add_column(r2_mask_col)  # Add vsky_counts column to the input table




t_filtered = t_all[~fdata2.mask]

t_filtered.write(table_name_output_filtered, format='fits', overwrite=True)
t_all.write(table_name_output_all, format='fits', overwrite=True)
print(table_name_output_all, 'SAVED!')



#hdu_filtered = fits.open(path_out + '/' + table_name_input_filtered)
table_in = t_filtered        #tTable(hdu_filtered[1].data)
table_out = table_in.copy()





images_tocombine = []
n = 0
for im in table_in:
    im_masked = im['im_normalized2'] + '_tocombine.fits'
    images_tocombine.append(im_masked)

    with fits.open(im['im_normalized2']) as hdul:

        #hdu_out_image = hdul['IMAGE']
        #hdu_out_prof = hdul['PROFILE']

        data = hdul['IMAGE'].data

        radii = (im['r1_mask'], im['r2_mask'])
        mask = rfa.sector_mask(data.shape, (int(data.shape[0]/2), int(data.shape[1]/2)), radii, angle_range=(0, 360))
        data[mask] = np.nan

        hdul['IMAGE'].data = data
        hdul.writeto(im_masked, overwrite=True)
        print(n, len(table_out), im_masked, radii)
    n = n + 1

im_tocombine_column = Column(images_tocombine, name='im_tocombine')
table_out.add_column(im_tocombine_column)  # Add vsky_counts column to the input table
table_out.write(output_file, format='fits', overwrite=True)
print(output_file, 'SAVED!')


