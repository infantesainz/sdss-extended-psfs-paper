# Filter galaxies from the set of objects for inner part of the PSF
#
# This script is the third iteration for building the inner part of the SDSS
# PSF. It compares the radial profile of each individual object with the
# stacked radial profile of the PSF obtained in the iteration 2. It computes
# the value of the standard deviation in the `check region' defined around
# the normalization radius and perform sigma clipping of these values.
# Objects with high value of `s' means that the radial profile differs a lot
# of the stacked radial profile PSF. They will be rejected in the final
# stacking process.
#
# Original author:
#     Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s):
# Copyright (C) 2019, Raul Infante-Sainz.
#
# This Python script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Python script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.


# Import mmodules
import os
import sys
import yaml
import argparse
import numpy as np
from astropy import wcs
from astropy.io import fits
import raulisfpy.rfastro as rfa
import raulisfpy.rfgeneral as rfg
from astropy.stats import sigma_clip
from astropy.table import Table,Column

# Matplotlib crashes in Mac OS systems
# To avoid this crash, use the condition:
from sys import platform as sys_pf
if sys_pf == 'darwin':
    import matplotlib
    matplotlib.use("TkAgg")

import matplotlib.pyplot as plt





# Reading parameters from command line
if True:

    parser = argparse.ArgumentParser(description='Reading arguments from command line')

    parser.add_argument('--band',
        help='SDSS filter band (u g r i z)')

    parser.add_argument('--starcat',
        help='Star catalogue')

    parser.add_argument('--yaml_cf',
        help='Configuration YAML file')

    parser.add_argument('--output_file',
        help='Output file data')

    parser.add_argument('--psf1_profile',
        help='PSF profile from iteration 1')

    args = parser.parse_args()


    # Define input files and parameters
    band                 = args.band
    im_psf               = args.psf1_profile
    configfile           = args.yaml_cf
    output_file          = args.output_file
    stars_catalogue      = args.starcat


    print('++++++++++++++++++++++++++++++')
    print('++++++++++++++++++++++++++++++')
    print('                              ')
    print('INPUT ARGUMENTS:', args        )
    print('                              ')
    print('++++++++++++++++++++++++++++++')
    print('++++++++++++++++++++++++++++++')


    # Read configuration file
    with open(configfile, 'r') as cfg_file:

        """ GENERAL PARAMETERS """
        cfg = yaml.load(cfg_file)
        pg  = cfg['GENERAL']

        pixelscale  = pg['pixelscale']
        rnormarcsec = pg['rnormarcsec']

        """ SPECIFIC PARAMETERS"""
        pl          = cfg[band]
        mag_kw      = pl['mag_kw']
        reject      = pl['bleedmasking']['reject']

        y1_mask     = pl['bleedmasking']['y1_mask']
        y2_mask     = pl['bleedmasking']['y2_mask']

        x1_left     = pl['bleedmasking']['x1_left']
        x2_left     = pl['bleedmasking']['x2_left']

        x1_center   = pl['bleedmasking']['x1_center']
        x2_center   = pl['bleedmasking']['x2_center']

        x1_right    = pl['bleedmasking']['x1_right']
        x2_right    = pl['bleedmasking']['x2_right']

plt.ioff()




table_input = fits.getdata(stars_catalogue, 1)
table_output = Table(table_input)

# Make the radial profile of the PSF
#if False:
#    data = fits.getdata(im_psf, 1)
#
#    profile = rfa.radial_profile(data, pixelscale, function='sigma_clip', rboundaries=(0, 'Rmax'), sigma_cut=3.0, center=None, symetrize=None, save_profile=im_psf_profile)
#    profile = profile['radial_profile']

rnormpix = int(rnormarcsec / pixelscale)
r_i = 2    # arcsec
r_o = 10  # arcsec

r_i = int(r_i/pixelscale)
r_o = int(r_o/pixelscale)

dpix = 1
dpixx = 3


p_psf = fits.getdata(im_psf, 1)
w1 = np.nanmedian(p_psf['smean'][r_i-dpix:r_i+dpix])
w2 = np.nanmedian(p_psf['smean'][r_o-dpix:r_o+dpix])




l_s = []
for row in table_input:
    im = row['im_normalized1']
    profile = fits.getdata(im, 'PROFILE')

    p_star = profile['smean'] / profile['smean'][rnormpix]

    ratio = p_star[r_i-dpixx:r_i+dpixx] / p_psf['smean'][r_i-dpixx:r_i+dpixx]
    fdata = ratio#sigma_clip(ratio)
    vmean = np.ma.mean(fdata)
    vstd = np.ma.std(fdata)

    l_s.append(vstd)

# Compute sigma clipping of distribuition of s values
s_filtered = sigma_clip(l_s, sigma_lower=10, sigma_upper=2.4, iters=None)

# Compute the maximun value good of s
s_maxgood = np.max(s_filtered.data[~s_filtered.mask])


# Create column with s values
c_s = Column(l_s, name='s')

# Create a column with all values equal to maximum value of s
c_smax = Column(len(c_s)*[s_maxgood], name='smaxgood')

table_output.add_column(c_s)
table_output.add_column(c_smax)

table_output.write(output_file, format='fits', overwrite=True)

"""
table_filtered = table_aux.copy()
n = 0
for row in table_filtered:
    if row['s'] > s_maxgood:
        print('REJECTED',row['im_normalized2'])
        table_filtered.remove_row(n)
    else:
        print(' ACEPTED',row['im_normalized2'])
    n = n + 1


table_filtered.add_column(Column(table_filtered['im_normalized2'], name='im_tocombine'))
table_filtered.write(output_file, format='fits', overwrite=True)
"""
