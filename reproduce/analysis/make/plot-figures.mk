# Build paper plots
#
# This Makefile is written to construct plots that will be presented in the
# SDSS PSFs paper. It is necessary some images and data that are under git
# control.
#
# Original author:
#     Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s):
# Copyright (C) 2019, Raul Infante-Sainz.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# Directories
# -----------
#
pplot-outdir = $(BDIR)/paper-plots




# Generate plot showing the rejection for bleeding stars criteria
# ---------------------------------------------------------------
#
# This plot show how stars are rejected if they have a strong depletion in
# the bleeding corrected pixels. It takes two stars and build the plot in
# which it can be seen the difference between a good star and a bad star.
ploted-rejecting-i-stars = $(pplot-outdir)/plot-rejecting-i-bleed-stars.pdf
pyplot-rejecting-i-stars = $(pythondir)/paperplot-rejecting-i-bleeding-stars.py
example-i-bad-star = $(INDIR)/example-of-bad-i-star.fits
example-i-good-star = $(INDIR)/example-of-good-i-star.fits
$(pplot-outdir): ; mkdir $@
$(ploted-rejecting-i-stars): $(example-i-good-star) $(example-i-bad-star) | $(pplot-outdir)
	python $(pyplot-rejecting-i-stars) $(word 1,$^) $(word 2,$^) $@





# Generate plot showing all 2-dimensional PSFs
# --------------------------------------------
#
# This plot shows the a gneral view of all 5 PSF images obtained. It is an
# array of 6 plots: g, r, i PSFs are shown in the upper panel while u, z and
# 2 circles showing 1 ad 8 arcmin radius scale are shown in the bottom panel.
# Since the order of plotting is changed, the prerequisites are the final
# PSF images but with the sorted properly to do the plot (g, r, i, u, z).
ploted-all-2d-psfs = $(pplot-outdir)/plot-all-2d-psfs.pdf
pyplot-all-2d-psfs = $(pythondir)/paperplot-all-2d-psfs.py
psfs-plot-order = $(foreach b, u g r i z, $(final-outdir)/psf-filter-$(b).fits)
$(pplot-outdir): ; mkdir $@
$(ploted-all-2d-psfs): $(psfs-plot-order) | $(pplot-outdir)
	# Pass all PSF images as an argument using quotes.
	python $(pyplot-all-2d-psfs) "$^" $@





# Plot profiles along and perpendicular to drift scanning direction
# ------------------------------------------------------------------
#
# This plot shows compare the flux along the drift scanning direction and
# the flux along the perpendicular direction to the drift scanning
# direction. In the same plot are the profiles for all SDSS bands.
ploted-driftscan-profile = $(pplot-outdir)/plot-drift-scanning-profile.pdf
pyplot-driftscan-profile = $(pythondir)/paperplot-drift-scanning-profile.py
psfs-plot-order = $(foreach b, u g r i z, $(final-outdir)/psf-filter-$(b).fits)
$(pplot-outdir): ; mkdir $@
$(ploted-driftscan-profile): $(psfs-plot-order) | $(pplot-outdir)
	# Pass all PSF images as an argument using quotes.
	python $(pyplot-driftscan-profile) "$^" $@





# Plot profiles comparation with de Jong 2008
# -------------------------------------------
#
# All PSF profiles are ploted together with de Jong 2008 profiles. To do
# that, Python script is used. Data correspond to de Jong 2008 SDSS profiles
# was kindly sent to me with the goal of making the comparation.
ploted-all-1d-psfs = $(pplot-outdir)/plot-all-1d-psfs.pdf
pyplot-all-1d-psfs = $(pythondir)/paperplot-all-1d-psfs.py
psfs-plot-order = $(foreach b, u g r i z, $(radial-profile-outdir)/psf-filter-$(b).fits)
psfs-dejong-profiles = $(foreach b, g r i, $(INDIR)/sdss_psf$(b).dat)
$(pplot-outdir): ; mkdir $@
$(ploted-all-1d-psfs): $(psfs-plot-order) $(psfs-dejong-profiles) | $(pplot-outdir)
	# Each set of profiles (this work and de Jong 2008) are passed as a
	# single argument each, by using quotes.
	python $(pyplot-all-1d-psfs) "$(word 1,$^) $(word 2,$^) $(word 3,$^) \
		                      $(word 4,$^) $(word 5,$^)" \
	                             "$(word 6,$^) $(word 7,$^) $(word 8,$^)" $@





# Plot histogram for rejectin objects according to star-galaxy `s' param
# ----------------------------------------------------------------------
#
# This plot shows the histogram of `s' parameter. It is a parameter which
# quantify the similarity of each inner object radial profile with the
# radial profile of the stacked PSF. The value of `s' should be low if the
# object radial profile is similar to the PSF radial profile, it means that
# it is a star. On the other hand, if the `s' value is high, it is not a
# star and then it should be rejected for the final combination of stars.
# This plot is the distribution of this `s' parameter.
ploted-inner-stars-histogram = $(pplot-outdir)/plot-rejecting-inner-stars-histogram.pdf
pyplot-inner-stars-histogram = $(pythondir)/paperplot-rejecting-inner-stars-histogram.py
$(pplot-outdir): ; mkdir $@
$(ploted-inner-stars-histogram): $(binner-outdir)/stats-iteration-3-filter-r-all.fits \
	                         | $(pplot-outdir)
	# Pass all PSF images as an argument using quotes.
	python $(pyplot-inner-stars-histogram) $< $@





# Plot inner profiles of PSF iteration 2, star, and galaxy
# --------------------------------------------------------
#
# This plot shows the radial profile of PSF iteration 2, a galaxy and a
# star. By using this plot it is clearly seen that there are some objects
# that are not stars and they are rejected for the final combination.
ploted-inner-stars-profiles = $(pplot-outdir)/plot-rejecting-inner-stars-profiles.pdf
pyplot-inner-stars-profiles = $(pythondir)/paperplot-rejecting-inner-stars-profiles.py
$(pplot-outdir): ; mkdir $@
$(ploted-inner-stars-profiles): $(binner-outdir)/psf-iteration-1-filter-r-profile.fits \
	                        $(INDIR)/star.fits \
	                        $(INDIR)/galaxy.fits \
	                        | $(pplot-outdir)
	# Pass all PSF images as an argument using quotes.
	python $(pyplot-inner-stars-profiles) $(word 1,$^) \
		                              $(word 2,$^) \
					      $(word 3,$^) \
					      $@





# Make color image of coma cluster: ORIGINAL
# ------------------------------------------
#
# Use a Python script to generate the color image, just print the line and
# not use the code.
ploted-coma-original = $(pplot-outdir)/coma-cluster-original-grey.png
coma-fits-dir = $(INDIR)
pyplot-coma-plots = $(pythondir)/rgb-from-irg-color-image.py
coma-original-panel = $(INDIR)/coma-cluster-original-grey.png
$(ploted-coma-original): $(coma-original-panel)

	# Generate the color image and color-grey image using i r g images
	echo " ----------------------------------------------------------- "
	echo " MAKE COLOR IMAGE OF COMA CLUSTER SCATTERED LIGHT SUBTRACTED "
	echo " ----------------------------------------------------------- "
	echo " python $(pyplot-coma-plots) \
                      $(coma-fits-dir)/coma-filter-i.fits \
	              $(coma-fits-dir)/coma-filter-r.fits \
	              $(coma-fits-dir)/coma-filter-g.fits \
                      $(pplot-outdir)/coma-cluster-original-color.png \
                      $(pplot-outdir)/coma-cluster-original-grey.png "
	echo "                                                             "
	echo " ----------------------------------------------------------- "

	# First, make a white rectangle, in which the line and text scale
	# will be written.
	irect=$(subst .png,-rect.png,$@)
	convert $< -fill white -draw "rectangle 10,50 240,90" $$irect


	# Add label "Scattered light" in the top left corner of the image
	ilabel=$(subst .png,-labeled.png,$@)
	convert $$irect \
	        -undercolor white -fill blue -flatten -pointsize 35 -annotate +10+80 '\     Original ' $$ilabel

	# Add a white space at the bottom of the image
	convert $$ilabel -background white -gravity south -splice 0x24 $@





# Make color image of coma cluster: SCATTERED LIGHT FIELD
# -------------------------------------------------------
#
#
# Use a Python script to generate the color image, just print the line and
# not use the code.
ploted-coma-slight = $(pplot-outdir)/coma-cluster-slight-grey.png
coma-slight-panel = $(INDIR)/coma-cluster-slight-grey.png
$(ploted-coma-slight): $(coma-slight-panel)

	# Generate the color image and color-grey image using i r g images
	echo " ----------------------------------------------------------- "
	echo " MAKE COLOR IMAGE OF COMA CLUSTER SCATTERED LIGHT SUBTRACTED "
	echo " ----------------------------------------------------------- "
	echo " python $(pyplot-coma-plots) \
                      $(coma-fits-dir)/coma-filter-i-slight.fits \
	              $(coma-fits-dir)/coma-filter-r-slight.fits \
	              $(coma-fits-dir)/coma-filter-g-slight.fits \
                      $(pplot-outdir)/coma-cluster-slight-color.png \
                      $(pplot-outdir)/coma-cluster-slight-grey.png "
	echo "                                                             "
	echo " ----------------------------------------------------------- "

	# First, make a white rectangle, in which the line and text scale
	# will be written.
	irect=$(subst .png,-rect.png,$@)
	convert $< -fill white -draw "rectangle 10,50 240,90" $$irect


	# Add label "Scattered light" in the top left corner of the image
	ilabel=$(subst .png,-labeled.png,$@)
	convert $$irect \
	        -undercolor white -fill blue -flatten -pointsize 35 -annotate +10+80 '\Scattered light' $$ilabel

	# Add white space at the bottom of the image
	convert $$ilabel -background white -gravity south -splice 0x24 $@





# Make color image of coma cluster: SUBTRACTED
# --------------------------------------------
#
#
# Use a Python script to generate the color image, just print the line and
# not use the code.
ploted-coma-sub = $(pplot-outdir)/coma-cluster-sub-grey.png
coma-fits-dir = reproduce/analysis/data/for-paper-plots/coma-cluster
coma-sub-panel = $(INDIR)/coma-cluster-sub-grey.png
$(ploted-coma-sub): $(coma-sub-panel)

	# Generate the color image and color-grey image using i r g images
	echo " ----------------------------------------------------------- "
	echo " MAKE COLOR IMAGE OF COMA CLUSTER SCATTERED LIGHT SUBTRACTED "
	echo " ----------------------------------------------------------- "
	echo " python $(pyplot-coma-plots) \
                      $(coma-fits-dir)/coma-filter-i-sub.fits \
	              $(coma-fits-dir)/coma-filter-r-sub.fits \
	              $(coma-fits-dir)/coma-filter-g-sub.fits \
                      $(pplot-outdir)/coma-cluster-sub-color.png \
                      $(pplot-outdir)/coma-cluster-sub-grey.png "
	echo "                                                             "
	echo " ----------------------------------------------------------- "




	# First, make a white rectangle, in which the line and text scale
	# will be written.
	irect1=$(subst .png,-rect.png,$@)
	convert $< -fill white -draw "rectangle 10,50 240,90" $$irect1


	# Add label "Scattered light" in the top left corner of the image
	ilabel=$(subst .png,-labeled.png,$@)
	convert $$irect1 \
	        -undercolor white -fill blue -flatten -pointsize 35 -annotate +10+80 '\Stars removed' $$ilabel

	# Add scale line at the bottom right of the image
	# In order to add the bar, note that the pixel scale of the figure
	# is pscale=2arcsec/pixel As a consequence, if the wanted longitude
	# of the bar is 5 arcmin, that means a longitude in pixels equal to
	# lpix = 5 arcmin * 60 arcsec/pixel / 2 arcssec/pixel = 150 pixels.
	# That is the reason of taking x0=930 and xf=1080.

	# First, make a white rectangle, in which the line and text scale
	# will be written.
	irect=$(subst .png,-rect.png,$@)
	convert $$ilabel -fill white -draw "rectangle 928,667 1082,717" $$irect


	# Second, draw the line
	iline=$(subst .png,-line.png,$@)
	convert $$irect -stroke red -strokewidth 6 -draw "line 930,710 1080,710" $$iline


	# Finally, add the text
	convert $$iline \
	        -fill red -pointsize 40 -style bold -annotate +930+700 '5 arcmin' $@







# Plot the Coma cluster of galaxies with and without stars: Javi's figure
# -----------------------------------------------------------------------
#
# This plot shows the coma cluster color image before and after of removing
# the stars by using the PSFs in the SDSS g, r and i bands. This plot is
# already saved and under git control, so this rule is for only copy that
# image into the $(BDIR)/paper-plot directory.
# The other plot shows how the calibration of the stars are carried out.
ploted-coma-cluster-javi = $(pplot-outdir)/plot-coma-cluster.pdf
$(ploted-coma-cluster-javi): $(INDIR)/plot_coma_panel.pdf \
	                      | $(pplot-outdir)
	cp $< $@

ploted-star-subtraction   = $(pplot-outdir)/plot-coma-cluster-star-subtraction.pdf
$(pplot-outdir): ; mkdir $@
$(ploted-star-subtraction): $(INDIR)/plot_star_fitting.pdf \
	                    | $(pplot-outdir)
	cp $< $@





# Copy to BDIR the plot of joining PSFs in r band
# -----------------------------------------------
#
# When joining the three parts of the PSF in one single PSF it has been
# created a plot with the radial profile of the three parts and the
# calibration done. Now, the r SDSS band plot of this calibration step is
# copied into the top BDIR plot directory. Since the target is the joined
# PSF and not the plot, the prerequisite is the joined PSF, and inside the
# rule the name is changed.
ploted-joining-filter-r = $(pplot-outdir)/plot-joining-filter-r.pdf
$(ploted-joining-filter-r): $(joined-outdir)/psf-filter-r.fits \
	                    | $(pplot-outdir)
	cp $(joined-outdir)/psf_outer-inter-inner_r_joined.pdf $@





# Copy to BDIR the plot of rejecting outer stars
# ----------------------------------------------
#
# When building the outer part of the PSF, some of them are rejected by
# plotting the Rmlim vs logvnorm2 and fitting a linear function to this
# data. Then, objects with more than 3-sigma clipped mean value to the
# fit are rejected. This plot is now copied to the BDIR plot directory
ploted-rejecting-filter-g-outer = $(pplot-outdir)/plot-rejecting-outer-stars-filter-g.pdf
$(ploted-rejecting-filter-g-outer): $(bouter-outdir)/stats-iteration-3-filter-g.fits \
	                    | $(pplot-outdir)
	cp $(bouter-outdir)/stats-iteration-3-filter-g-plot.pdf $@





# Make small the size of the figures
# ----------------------------------
#
# Paper plots should not be so heavy. In order to make them more light, they
# are downgraded
original-paper-plots = $(ploted-rejecting-filter-g-outer) \
	               $(ploted-inner-stars-histogram) \
	               $(ploted-inner-stars-profiles) \
	               $(ploted-driftscan-profile) \
	               $(ploted-rejecting-i-stars) \
		       $(ploted-star-subtraction) \
		       $(ploted-joining-filter-r) \
	               $(ploted-all-1d-psfs) \
	               $(ploted-all-2d-psfs)
# Define the downsized figures in a new directory
downsize-plots-dir = $(texdir)/figures
downsize-paper-plots = $(foreach plot, $(original-paper-plots), \
	                $(downsize-plots-dir)/$(notdir $(plot)))
$(downsize-plots-dir): ; mkdir $@
$(downsize-paper-plots): $(downsize-plots-dir)/%: $(pplot-outdir)/% \
	                                          | $(downsize-plots-dir)

	# Use Ghostscript to change the quality in order to keep the size of
	# the figures as small as possible
	gs -dCompatibilityLevel=1.4 \
	   -dPDFSETTINGS=/prepress \
	   -sDEVICE=pdfwrite \
	   -dNOPAUSE \
	   -dQUIET \
	   -dBATCH \
	   -sOutputFile=$@ $<





# Merge all three coma cluster plots in one single figure
# -------------------------------------------------------
#
# Use the three panels (original, scattered light, subtracted) of Coma
# cluster to make a single figure showing them. Save the output directly
# into the final downsized figures directory.
ploted-coma-cluster-raul = $(texdir)/figures/plot-coma-cluster.png
$(ploted-coma-cluster-raul): $(ploted-coma-original) \
	                     $(ploted-coma-slight) \
	                     $(ploted-coma-sub)

	# Use convert to concatenate the 3 figures
	convert $^ -append -resize 512 -quality 75 $@





# Final TeX macro
# ---------------
#
# Make an empty .tex file as final file when all plots have been generated.
# been generated.
$(mtexdir)/plot-figures.tex: $(downsize-paper-plots) \
                             $(ploted-coma-original) \
                             $(ploted-coma-slight) \
		             $(ploted-coma-cluster-raul) \
                             $(ploted-coma-sub) | $(mtexdir)

	# `NSTARS' Number of stars considered
	echo "\newcommand{\nstars}{$(NSTARS)}" > $@

	# `s' value for the good star
	v=$$(awk '/^STAR/{printf "%.2f", $$3}' $(pplot-outdir)/plot-rejecting-inner-stars-profiles.dat)
	echo "\newcommand{\svalueforstar}{$$v}" >> $@

	# `s' value for the bad galaxy
	v=$$(awk '/^GALAXY/{printf "%.2f", $$3}' $(pplot-outdir)/plot-rejecting-inner-stars-profiles.dat)
	echo "\newcommand{\svalueforgalaxy}{$$v}" >> $@

	# `s' value for distinguish between stars and galaxies
	v=$$(awk 'NR==2{printf "%.2f", $$1}' $(pplot-outdir)/plot-rejecting-inner-stars-histogram.dat)
	echo "\newcommand{\svaluefordip}{$$v}" >> $@

	# Mean slope of all PSF profiles
	v=$$(awk 'FNR > 1{ sum += $$3; n++ } END { if (n > 0) print sum / n; }' \
	     $(pplot-outdir)/plot-all-1d-psfs.dat | awk '{printf "%.2f", $$1}')
	echo "\newcommand{\meanslopevalue}{$$v}" >> $@

	# u-band excess of flux in drift scanning direction
	v=$$(awk 'NR==2{printf "%.1f", $$2}' $(pplot-outdir)/plot-drift-scanning-profile.dat)
	echo "\newcommand{\ufluxexcess}{$$v}" >> $@

	# g-band excess of flux in drift scanning direction
	v=$$(awk 'NR==3{printf "%.1f", $$2}' $(pplot-outdir)/plot-drift-scanning-profile.dat)
	echo "\newcommand{\gfluxexcess}{$$v}" >> $@

	# r-band excess of flux in drift scanning direction
	v=$$(awk 'NR==4{printf "%.1f", $$2}' $(pplot-outdir)/plot-drift-scanning-profile.dat)
	echo "\newcommand{\rfluxexcess}{$$v}" >> $@

	# z-band excess of flux in drift scanning direction
	v=$$(awk 'NR==6{printf "%.1f", $$2}' $(pplot-outdir)/plot-drift-scanning-profile.dat)
	echo "\newcommand{\zfluxexcess}{$$v}" >> $@
