import numpy as np
import matplotlib.pyplot as plt

from astropy.io import fits
from astropy.utils.data import get_pkg_data_filename
from astropy.convolution import Gaussian2DKernel
from scipy.signal import convolve as scipy_convolve
from astropy.convolution import convolve, Gaussian1DKernel, Gaussian2DKernel, interpolate_replace_nans
import sys
from astropy.stats import sigma_clip
import os
from scipy.ndimage.interpolation import rotate
import yaml


print('++++++++++++++++++++++++++++++++++')
print('                                  ')
print(sys.argv)
print('                                  ')
print('++++++++++++++++++++++++++++++++++')

configfile = sys.argv[1]
psf_input = sys.argv[2]
psf_interpolated = sys.argv[3]


with open(configfile, 'r') as cfg_file:
    """ INPUT PARAMETERS """

    cfg = yaml.load(cfg_file)
    pg = cfg

    ## PATHS AND MODULES
    band = pg['band']

    path_module = pg['path_module']
    #path_out = pg['path_out']
    path_in = pg['path_in']

    sys.path.append(path_module)
    import raulisfpy.rfastro as rfa
    import raulisfpy.rfgeneral as rfg
    #import sdss_psf_pyfunctions as rfa
    sys.path.remove(path_module)
    ##

    pixscale = pg['pixelscale']


    version = pg['version']
    rmax = pg['rmax']
    y_i = pg['y_i']#1490
    y_f = pg['y_f']#1510

    x_i = pg['x_i']#1400
    x_f = pg['x_f']#1600


if band != 'i':
    angles = pg['angles'] # [-10, +10]

    #im_psf_interpolated = path_out + '/' + 'sdss_psf_'+band+'-band_interpolated.fits'
    #im_psf_interpolated_rotated_cuted_FINAL_IMAGE = path_out + '/' + 'sdss_psf_'+band+'-band_'+version+'_image.fits'
    #im_psf_interpolated_rotated_cuted_FINAL_PROFILE = path_out + '/' + 'sdss_psf_'+band+'-band_'+version+'_profile.fits'


    ## INTREPOLATION
    with fits.open(psf_input) as hdu_psf:
        AJ = hdu_psf[0].header['AJ']
        CJ = hdu_psf[0].header['CJ']

        dat_psf = hdu_psf[0].data
        data_interpolated = dat_psf.copy()
        dat_psf[np.isnan(dat_psf)] = 0
        dat_psf[y_i+5:y_f-5, :] = 0

        cube = []
        for angle in angles:
            if angle == 0: continue
            print('ROTATING ANGLE', angle)
            rotated1 = rotate(dat_psf, angle, axes=(1, 0), reshape=False, output=None, order=3, mode='constant', cval=0.0, prefilter=True)
            rotated1[rotated1 == 0] = np.nan
            cube.append(rotated1)

        cube = np.array(cube)
        stacked = np.nanmedian(cube, axis=0)

        data_interpolated[y_i:y_f, :x_i] = stacked[y_i:y_f, :x_i]
        data_interpolated[y_i:y_f, x_f:] = stacked[y_i:y_f, x_f:]

        hdu_out = fits.PrimaryHDU(data_interpolated.astype(np.float32))
        hdu_out.writeto(psf_interpolated, overwrite=True)


elif band == 'i':
    #im_psf_interpolated_rotated_cuted_FINAL_IMAGE = path_out + '/' + 'sdss_psf_'+band+'-band_'+version+'_image.fits'
    #im_psf_interpolated_rotated_cuted_FINAL_PROFILE = path_out + '/' + 'sdss_psf_'+band+'-band_'+version+'_profile.fits'
    with fits.open(psf_input) as hdu:
        AJ = hdu[0].header['AJ']
        CJ = hdu[0].header['CJ']
        indata = hdu[0].data
        data_z = indata.copy()
        data_interpolated = indata.copy()
        g = Gaussian1DKernel(stddev=7)
        for  icol in range(indata.shape[0]):
            if x_i < icol < x_f:
                continue
            print(icol, indata.shape[0])
            y_ori = data_z[:,icol]
            x_ori = np.arange(len(y_ori))

            ## Convolved data
            y_conv1 = convolve(y_ori, g)
            diff = y_ori - y_conv1

            fdata = sigma_clip(diff, sigma=3.0)
            y_masked1 = y_ori
            y_masked1[fdata.mask] = np.nan

            y_conv2 = convolve(y_masked1, g)

            data_interpolated[y_i:y_f, icol] = y_conv2[y_i:y_f]
    hdu_out = fits.PrimaryHDU(data_interpolated.astype(np.float32))
    hdu_out.writeto(psf_interpolated, overwrite=True)


else:
    print('>band< has to be one of the following:  u,g,r,i,z')
    sys.exit('STOPPED')

#####
####### ROTATE 90 DEGREES, CUT AND PUT HEADER
#####with fits.open(im_psf_interpolated) as hdu_psf:
#####    """"""
#####
#####    data = hdu_psf[0].data
#####    data[np.isnan(data)] = 0
#####    radii = (0, rmax)
#####    center = (int(data.shape[0]/2), int(data.shape[1]/2))
#####
#####    mask = rfa.sector_mask(data.shape, center, radii, angle_range=(0, 360))
#####
#####    data[~mask] = np.nan
#####    sumallpixels = np.nansum(data)
#####    data = data / sumallpixels
#####
#####    subarray = data[center[0]-rmax:center[0]+rmax+1,center[1]-rmax:center[1]+rmax+1]
#####
#####    subarray_rotated = np.rot90(subarray, k=3, axes=(0, 1))
#####    hdu_out = fits.PrimaryHDU(subarray_rotated.astype(np.float32))
#####    hdu_out.header.set('BAND',      band, 'SDSS band')
#####    hdu_out.header.set('SCALE',     pixscale, 'Pixel scale of SDSS images (arcsec/pixel)')
#####    hdu_out.header.set('NORM',      sumallpixels, 'Sum(all pixels)')
#####    hdu_out.header.set('RI',        CJ, 'Radius inner-inter (pixel)')
#####    hdu_out.header.set('RO',        AJ, 'Radius inter-outer (pixel)')
#####    hdu_out.header.set('VERSION',   version, 'Version number')
#####    hdu_out.header.set('COMMENT',   'Drift scanning direction is from left to right')
#####    hdu_out.header.set('CONTACT1',  'rinfante(at)iac.es', 'Contact e-mail')
#####    hdu_out.header.set('CONTACT2',  'infantesainz(at)gmail.com', 'Contact e-mail')
#####
#####    hdu_out.verify('fix')
#####
#####    hdu_out.writeto(im_psf_interpolated_rotated_cuted_FINAL_IMAGE, overwrite=True)
#####
#####if os.path.isfile(im_psf_interpolated_rotated_cuted_FINAL_PROFILE) == False:
#####    """ COMPUTE RADIAL PROFILE """
#####    p_stamp_all = rfa.radial_profile(subarray_rotated, pixscale, function='sigma_clip', rboundaries=(0, rmax), sigma_cut=3.0, center=None, symetrize=None, save_profile=im_psf_interpolated_rotated_cuted_FINAL_PROFILE)
#####
