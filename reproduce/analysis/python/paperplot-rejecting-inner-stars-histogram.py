# Plot the distribution of `s' values
#
# This Python script makes the plot of the histogram of `s' parameters. It
# is a parameter that was previously saved in the last iteration for the
# inner part objects. The parameter `s' is a measure of how similar are the
# radial profiles of individual objects to the radial profile of the stacked
# final image. It means that if an object has low value of `s' then it is
# similar to the final stacked image. On the other hand, if an object has
# very different (high) value of `s' that means that it is not similar to
# the stacked image, so it is not a star and it should be rejected for the
# final combination. This Python script build the plot in which the
# distribution of `s' is plotted.
#
# Original author:
#     Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s):
# Copyright (C) 2019, Raul Infante-Sainz.
#
# This Python script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Python script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# Import modules
import sys
import numpy as np
from astropy.io import fits
from astropy.stats import sigma_clip

# Matplotlib crashes in Mac OS systems
# To avoid this crash, use the condition:
from sys import platform as sys_pf
if sys_pf == 'darwin':
    import matplotlib
    matplotlib.use("TkAgg")
import matplotlib.pylab as plt
plt.style.use('classic')





# Read input arguments
input_table   = sys.argv[1]    # Input data table
output_plot   = sys.argv[-1]   # Target, name of the final plot

# Read input table
table = fits.getdata(input_table)

# Read the parameter s and compute take the log10 of them
s = table['s']
slog = np.log10(s)
s_filtered = sigma_clip(slog, sigma_lower=10, sigma_upper=2.5, iters=None)

# Accepted and rejected points
good = s[~s_filtered.mask]
baad = s[s_filtered.mask]

# s values that are not nans
snonan = s[~np.isnan(s)]

# Compute the histogram, not for plotting, only for obtaining bins
plt.clf()
hist, bins, _ = plt.hist(snonan, bins='sqrt')

# Compute bins in log scale
# Number of bins is manually selected to 80 because,
# considering 1050 stars from scratch the plot looks fine.
logbins = np.logspace(np.log10(bins[0]), np.log10(bins[-1]), len(bins))

# Make the plots
plt.clf()
fig = plt.figure()
#plt.grid()
ax = fig.add_subplot(111)

ax.hist(good, bins=logbins, color='b', alpha=0.9, \
         label='Point-like sources', histtype='stepfilled', hatch='//')

ax.hist(baad, bins=logbins, color='r', alpha=0.9, \
         label='Extended sources', histtype='stepfilled', hatch='\\\\')

# Decorate the plot
ax.set_ylabel('Number of images', fontsize=16)
ax.set_xlabel('s', fontsize=16)
ax.set_xscale('log')
ax.set_xlim(0.005, 3)

axticks = [0.01, 0.1, 1]
axticklabels = [0.01, 0.1, 1]

ax.set_xticks(axticks)
ax.set_xticklabels(axticklabels)

plt.legend()
#plt.grid()


# Save the figure
plt.savefig(output_plot, format='pdf', bbox_inches='tight')
print(output_plot, 'SAVED!')

# Compute the median of the distribution
data_out = [10**np.ma.median(s_filtered)]
data_out = np.array(data_out)


# Save .txt file with data
statistics_outfile = output_plot.replace('.pdf','.dat')
np.savetxt(statistics_outfile, data_out, fmt="%s", \
           header="MEDIAN")

