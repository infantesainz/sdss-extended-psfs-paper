# Joining outer, inter and inner part of the PSF
#
# This Makefile is written to joing the different parts of the PSF
#
# Original author:
#     Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s):
# Copyright (C) 2019, Raul Infante-Sainz.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# Directories
# -----------
#
binter-indir = $(BDIR)
joined-outdir = $(BDIR)/joined-parts





# Joninig PSF parts
# -----------------
#
pyscript-joining = $(pythondir)/joining-parts.py
joined-psfs = $(foreach b,$(BANDS),$(joined-outdir)/psf-filter-$(b).fits)
$(joined-outdir): ; mkdir $@
$(joined-psfs): $(joined-outdir)/psf-%.fits: \
                         $(pyscript-joining) \
                         $(pyconfigdir)/joining-%.yaml \
                         $(bouter-outdir)/psf-iteration-3-%.fits \
                         $(binter-outdir)/psf-iteration-3-%.fits \
                         $(binner-outdir)/psf-iteration-3-%.fits \
                         $(astcrop-cfgfile) | $(joined-outdir)
	python $^ $(joined-outdir) $@






# Final TeX macro
# ---------------
#
# Make an empty .tex file as final file when all light images
# bias, dark and gain corrected have been obtained
#
$(mtexdir)/joining-parts.tex: $(joined-psfs) | $(mtexdir)
	echo "%empty" > $@

