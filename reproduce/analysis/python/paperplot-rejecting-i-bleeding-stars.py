# Generate plot of bleeding rejection criteria
#
# This Python script makes the plot in which it is shown how the stars with
# a strong depletion in the bleeding / saturated zone is done. It plot an
# example of one bad stars in an upper panel, and the collapsed profiles in
# the bottom panel.
#
# Original author:
#     Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s):
# Copyright (C) 2019, Raul Infante-Sainz.
#
# This Python script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Python script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.






# Import modules
import sys
import numpy as np
from astropy.io import fits

# Matplotlib crashes in Mac OS systems
# To avoid this crash, use the condition:
from sys import platform as sys_pf
if sys_pf == 'darwin':
    import matplotlib
    matplotlib.use("TkAgg")

import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.gridspec as gridspec
from matplotlib.colors import LogNorm, SymLogNorm
plt.style.use('classic')



# Input files and parameters
im_good = sys.argv[1]
im_baad = sys.argv[2]
im_out = sys.argv[3]


# Define different regions over the star
dx = 600
dy = 500

y1_mask=1200
y2_mask=1800

x1_left=1450
x2_left=1490
x1_center=1495
x2_center=1505
x1_right=1510
x2_right=1550


# Define plotting parameters
plt.clf()
plt.ion()

gs = gridspec.GridSpec(9, 1)#, height_ratios=[4,1])
ax0 = plt.subplot(gs[:6])
ax1 = plt.subplot(gs[6:])
axs = [ax0, ax1]


# Read image data: good star
dat_good = fits.getdata(im_good, 0)
dat_good_original = dat_good.copy()
shape = dat_good.shape
cy = int(shape[0]/2)
cx = int(shape[1]/2)


dat_good[y1_mask:y2_mask,:] = np.nan

p1_good = np.nanmedian(dat_good, axis=0)
right_good = p1_good[x1_right:x2_right]
center_good = p1_good[x1_center:x2_center]
left_good = p1_good[x1_left:x2_left]

fa_good = np.nanmin((np.nanmin(left_good), np.nanmin(right_good))) / np.nanmin(center_good)
dat_good = dat_good[cy-dy:cy+dy, cx-dx:cx+dx]


# Read image data: bad star
dat_baad = fits.getdata(im_baad, 0)
dat_baad_original = dat_baad.copy()
dat_baad[y1_mask:y2_mask,:] = np.nan


p1_baad = np.nanmedian(dat_baad, axis=0)
right_baad = p1_baad[x1_right:x2_right]
center_baad = p1_baad[x1_center:x2_center]
left_baad = p1_baad[x1_left:x2_left]

fa_baad = np.nanmin((np.nanmin(left_baad), np.nanmin(right_baad))) / np.nanmin(center_baad)
dat_baad_sub = dat_baad_original[:, cx-dx:cx+dx]

print('s: ', fa_baad, im_baad)
print('s: ', fa_good, im_good)

axs[0].imshow(dat_baad_sub, origin='lower', vmin=-0.175, vmax=0.45)#interpolation='none',
#axs[0].set_cmap('nipy_spectral')
plt.set_cmap('gist_ncar')
axs[0].set_xlim(0, 2*dx)
axs[0].set_ylim(1100, 2500)
rect = patches.Rectangle((6, 1200), 2*dx-12, 600, linewidth=6, edgecolor='m', \
                         facecolor='none', linestyle='solid', hatch='X', fill=True, alpha=1.0)


axs[0].add_patch(rect)


yp1_good = p1_good[cx-dx:cx+dx]
yp1_baad = p1_baad[cx-dx:cx+dx]
p1shape = dat_good.shape
p1cy = int(p1shape[0]/2)
p1cx = int(p1shape[1]/2)

axs[1].plot(yp1_good, linestyle='solid', color='b',  linewidth=2, alpha=0.9, label='Good star')
axs[1].plot(yp1_baad, linestyle='dotted', color='g', linewidth=2, alpha=0.9, label='Bad star')



axs[1].axvline(p1cx - 50,      0, 1,            color='k',  linewidth=1.5,  alpha=1)
axs[1].axvline(p1cx,           0, 1,            color='r',  linewidth=1.5,  alpha=1)
axs[1].axvline(p1cx + 50,      0, 1,            color='r',  linewidth=1.5,  alpha=1)
axs[1].axvline(p1cx + 50 + 50, 0, 1,            color='k',  linewidth=1.5,  alpha=1)

axs[1].axvspan(p1cx - 50, p1cx,            facecolor='k',  alpha=0.5, \
               label='Left and Right', hatch='//')

axs[1].axvspan(p1cx,      p1cx + 50,       facecolor='r',  alpha=0.5, \
               label='Central', hatch='\\\\')

axs[1].axvspan(p1cx + 50, p1cx + 50 + 50,  facecolor='k',  alpha=0.5, \
               hatch='//')

axs[1].set_xlabel('Distance from centre [pixels]', fontsize=16)
axs[1].set_xticklabels(np.arange(-600, 600+200, 200))

axs[1].set_ylabel('Median flux', fontsize=16)

ymin, ymax = axs[1].get_ylim()
axs[1].set_yticks(np.round(np.linspace(ymin, ymax, 5), 1))



axs[1].grid()
axs[1].legend(prop={'size':9})

axs[0].vlines(p1cx - 50,      1100,   1200,  colors='k', linewidth=1.5, linestyles='solid')
#axs[0].vlines(p1cx - 50,      1200,   1800,  colors='m', linewidth=1.5, linestyles='dashed')
axs[0].vlines(p1cx - 50,      1800,   2550,  colors='k', linewidth=1.5, linestyles='solid')


axs[0].vlines(p1cx,      1100,   1200,  colors='r', linewidth=1.5, linestyles='solid')
#axs[0].vlines(p1cx,      1200,   1800,  colors='m', linewidth=1.5, linestyles='dashed')
axs[0].vlines(p1cx,      1800,   2550,  colors='r', linewidth=1.5, linestyles='solid')



axs[0].vlines(p1cx + 50,      1100,   1200,  colors='r', linewidth=1.5, linestyles='solid')
#axs[0].vlines(p1cx + 50,      1200,   1800,  colors='m', linewidth=1.5, linestyles='dashed')
axs[0].vlines(p1cx + 50,      1800,   2550,  colors='r', linewidth=1.5, linestyles='solid')


axs[0].vlines(p1cx + 50 + 50,      1100,   1200,  colors='k', linewidth=1.5, linestyles='solid')
#axs[0].vlines(p1cx + 50 + 50,      1200,   1800,  colors='m', linewidth=1.5, linestyles='dashed')
axs[0].vlines(p1cx + 50 + 50,      1800,   2550,  colors='k', linewidth=1.5, linestyles='solid')





# Final plot parameters and save figure
yticks = np.arange(-500, 1000, 200)

axs[0].set_yticklabels(yticks)
axs[0].set_xticklabels([])
axs[0].set_ylabel('Distance from centre [pixels]', fontsize=16)
axs[0].set_aspect('auto')

plt.subplots_adjust(hspace=0.4, wspace=0)
plt.savefig(im_out, format='PDF', bbox_inches='tight')
print(im_out, 'SAVED!')
