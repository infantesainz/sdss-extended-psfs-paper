# Generate plot of all PSF images
#
# This Python script makes the plot in which all PSF 2-dimensonal images are
# shown. The plot consists in 6 different subplots, each one for one PSF
# filter, and the last one is a couple of circles with the aim of showing
# the size of the PSFs (radius of 1 and 8 arcmin)
#
# Original author:
#     Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s):
# Copyright (C) 2019, Raul Infante-Sainz.
#
# This Python script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Python script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.






# Import modules
import sys
import numpy as np
from astropy.io import fits

# Matplotlib crashes in Mac OS systems
# To avoid this crash, use the condition:
from sys import platform as sys_pf
if sys_pf == 'darwin':
    import matplotlib
    matplotlib.use("TkAgg")

import matplotlib.pyplot as plt
from matplotlib.patches import Circle
import matplotlib.gridspec as gridspec
from matplotlib.colors import LogNorm, SymLogNorm
from mpl_toolkits.axes_grid1 import ImageGrid, make_axes_locatable
plt.style.use('classic')





# Read input arguments
psfims_aux    = sys.argv[1]    # All PSF images
output_plot   = sys.argv[-1]   # Target, name of the final plot

# First argument is all PSF images separated by a space, it is necessary to
# construct a list from this string. To do that, use `.split()'. After that,
# append a white space to be able to create last plot (with the two
# circles).
psfims = psfims_aux.split()
psfims.append(' ')


# Define plotting parameters
c = 2000
zp = 22.5
pixelscale = 0.396

f_arcmin_to_pix = 60.0/0.396
ticks_arcmin = np.array([-9, -7, -5, -3, -1, 0, 1, 3, 5, 7, 9])
ticks_labels = np.array(['9', '7', '5', '3', '1', '0', '1', '3', '5', '7', '9'])
ticks_labels_n = np.array(['', '', '', '', '', '', '', '', '', '', ''])

ticks_pixels = ticks_arcmin * f_arcmin_to_pix
rtot = int(ticks_pixels[-1])

ticks_pixels = ticks_pixels + rtot



plt.clf()
plt.close('all')
plt.ion()

fig= plt.figure()

nrow = 2
ncol = 3

grid = ImageGrid(fig, 111,
                 nrows_ncols=(nrow, ncol),
                 direction='row',
                 axes_pad=0.05,
                 share_all=True,
                 cbar_pad=0.001,
                 cbar_location='bottom',
                 cbar_mode='single',
                 cbar_size='3%',
                 label_mode='L',
                 add_all=True
                 )


plt.set_cmap('nipy_spectral')
plt.set_cmap('gist_ncar')
#plt.set_cmap('jet')

# Order of plotting is not u, g, r, i, z !!
filters = ['u', 'g', 'r', 'i', 'z', '']
n = 0
for ax_image, filt, psfim in list(zip(grid, filters, psfims)):

    print(' ')
    print('FILTER', filt)

    vmin = 7e-11
    vmax = 7e-06

    # If filt is empty, plot the two circles
    if filt == '':
        new = np.nan*alldata

        im_nan = ax_image.imshow(new, interpolation='none', origin='lower', \
                             norm=SymLogNorm(linthresh=vmin, vmin=vmin, vmax=vmax))
        circ1 = Circle((2425/2,2425/2),  151,  linewidth=2,   facecolor='none')
        circ8 = Circle((2425/2,2425/2),  1212, linewidth=2,   facecolor='none')
        ax_image.add_patch(circ1)
        ax_image.add_patch(circ8)

        ax_image.annotate("1'", xy=(2425/2+151,      2425/2+151), fontsize=16)
        ax_image.annotate("8'", xy=(2425/2+875, 2425/2+875), fontsize=16)
        ax_image.arrow(0.25*2425, 0.25*2425, 2*0.25*2425, 0, head_width=150, \
                       facecolor='k', linewidth=3, length_includes_head=True)

    # If it is a filter, plot the array data
    else:
        alldata = fits.getdata(psfim, 0)

        im = ax_image.imshow(alldata, interpolation='none',origin='lower', \
                             norm=SymLogNorm(linthresh=vmin, vmin=vmin, vmax=vmax))


    ax_image.annotate(filt, xy=(100, 2050), fontsize=18)
    ax_image.set_xticklabels([])
    ax_image.set_yticklabels([])

    n = n + 1

# Plot color bar and save figure
cb = fig.colorbar(im_nan, cax=grid[0].cax, orientation='horizontal')
plt.savefig(output_plot, format='pdf', bbox_inches='tight')
print(output_plot, 'SAVED!')
