# Create stamps for inner part of the PSF: iteration 1
#
# This script is the first iteration for building the inner part of the SDSS
# PSF. It downloads SDSS from an input catalogue that has been previously
# prepared, and then it performs some steps:
# - Recenter each image where the bright star is
# - Mask all objects except the bright star
# - Compute the radial profile and normalize in a given radius
# - Save the normalized and masked image, and also the statistics.
#
# Original author:
#     Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s):
# Copyright (C) 2019, Raul Infante-Sainz.
#
# This Python script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Python script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.


# Import mmodules
import os
import sys
import yaml
import argparse
import numpy as np
from astropy import wcs
from astropy.io import fits
import raulisfpy.rfastro as rfa
import raulisfpy.rfgeneral as rfg
from astropy.table import Table,Column




# Reading parameters from command line
if True:

    parser = argparse.ArgumentParser(description='Reading arguments from command line')

    parser.add_argument('--band',
        help='SDSS filter band (u g r i z)')

    parser.add_argument('--nmax', type=int,
        help='Max number of images')

    parser.add_argument('--starcat',
        help='Star catalogue')

    parser.add_argument('--yaml_cf',
        help='Configuration YAML file')

    parser.add_argument('--swarp_cf',
        help='SWarp configuration file')

    parser.add_argument('--noisechisel_cf',
        help='NoiseChisel configuration file')

    parser.add_argument('--segment_cf',
        help='Segment configuration file')

    parser.add_argument('--mkcatalog_cf',
        help='MakeCatalog configuration file')

    parser.add_argument('--output_file',
        help='Output file data')

    parser.add_argument('--path_out',
        help='Out path where all output files will be saved')

    args = parser.parse_args()


    # Define input files and parameters
    nmax                 = args.nmax
    band                 = args.band
    path_out             = args.path_out
    configfile           = args.yaml_cf
    output_file          = args.output_file
    stars_catalogue      = args.starcat
    swarp_configfile     = args.swarp_cf
    segment_cfg_file     = args.segment_cf
    noisechisel_cfg_file = args.noisechisel_cf
    mkcatalogue_cfg_file = args.mkcatalog_cf


    print('++++++++++++++++++++++++++++++')
    print('++++++++++++++++++++++++++++++')
    print('                              ')
    print('INPUT ARGUMENTS:', args        )
    print('                              ')
    print('++++++++++++++++++++++++++++++')
    print('++++++++++++++++++++++++++++++')


    # Read configuration file
    with open(configfile, 'r') as cfg_file:

        """ GENERAL PARAMETERS """
        cfg = yaml.load(cfg_file)
        pg  = cfg['GENERAL']

        dimpix      = pg['dimpix']
        pixelscale  = pg['pixelscale']
        rnormarcsec = pg['rnormarcsec']

        """ SPECIFIC PARAMETERS"""
        pl          = cfg[band]
        mag_kw      = pl['mag_kw']
        reject      = pl['bleedmasking']['reject']

        y1_mask     = pl['bleedmasking']['y1_mask']
        y2_mask     = pl['bleedmasking']['y2_mask']

        x1_left     = pl['bleedmasking']['x1_left']
        x2_left     = pl['bleedmasking']['x2_left']

        x1_center   = pl['bleedmasking']['x1_center']
        x2_center   = pl['bleedmasking']['x2_center']

        x1_right    = pl['bleedmasking']['x1_right']
        x2_right    = pl['bleedmasking']['x2_right']


# Define the radius of normalization in pixels
rnormpix = int(rnormarcsec / pixelscale)

# The input catalogue of stars has the following columns:
# USNO-B1.0_star, RAJ2000_star, DEJ2000_star, e_RAJ2000_star,
# e_DEJ2000_star, B1mag_star, R1mag_star, B2mag_star, R2mag_star, Imag_star,
# col1_field, ra_field, dec_field, rerun_field, run_field, camcol_field,
# field_field

# Sort star catalogue by selected magnitude
tt = Table.read(stars_catalogue, format='fits')
i = np.argsort(tt[mag_kw])
t = tt[i]

# Define the output statistics table
# Empty lists defined will form the output table
stats = Table()

l_ra = []
l_de = []
l_mag = []
l_vnorm = []
l_lpath = []
l_logvnorm = []
l_rnormpix = []
l_rnormarcsec = []
l_image_swarp = []
l_im_normalized = []

# Define two counter: total number of stars, and number of good stars.
n = 0
n_good = 0
while n_good < nmax:
    """ MAIN LOOP """

    print(' ')
    print('#######################################')
    print('Trying', n, 'NGood', n_good, 'of ', nmax)
    print('#######################################')
    print(' ')

    # Empty `rmlist' each time new star is considered
    rmlist = []

    # Define all kind of images
    image_fits = 'None'
    image_swarp = 'None'
    im_detected = 'None'
    im_segmented = 'None'
    im_normalized = 'None'
    image_masked1 = 'None'
    im_noisechisel = 'None'

    ## Read data and info of each star
    star  = t[n]
    mag   = star[mag_kw]
    ra    = star['RAJ2000_star']
    de    = star['DEJ2000_star']
    rerun = star['rerun_field']
    run   = star['run_field']
    camcol= star['camcol_field']
    field = star['field_field']

    # Define images in the output directory
    image_swarp = path_out + '/' + \
                  f'frame-{band}-{run:06}-{camcol}-{field:04}.fits_swarp.fits'
    image_fits = path_out + '/' + \
                 f'frame-{band}-{run:06}-{camcol}-{field:04}.fits'

    # If the recentered image doesn't exist, then download
    if os.path.isfile(image_fits) == False:
        # Download and decompress the SDSS image
        image_fits = rfa.sdss_download_dr13(band, rerun, run, camcol, field, \
                     path_out, download=True, decompress=True)['im_name']

        # Create and overwrite the image WITHOUT the header
        data_sci = fits.getdata(image_fits, 0)
        header_sci = fits.getheader(image_fits, 0)

        hdu_out = fits.PrimaryHDU(data_sci.astype(np.float32))
        hdu_out.writeto(image_fits, overwrite=True)

        # Read WCS information and transform RA DEC from USNO-B1 catalogue
        # to pixel values on this image.
        w = wcs.WCS(header_sci)
        x, y = w.all_world2pix(ra, de, 0)

        # Run Noisechisel + Segment + MakeCatalog to obtain the center
        if False:
            # Obtain the center of the star by using:
            # `astnoisechisel' + `astegment' + `astmkcatalog'
            print('')
            print('DETERMINING CENTER OF THE STAR')
            print('')
            imaux_detected = image_fits + '_detected.fits'
            imaux_segmented = image_fits + '_segmented.fits'
            imaux_catalogue = image_fits + '_catalogue.fits'
            tilesize = '100,100'
            qthresh = '0.5'
            try:
                # Noisechisel
                line_noisechisel = ('astnoisechisel --hdu=0 ' + image_fits +
                                    ' --output=' + imaux_detected +
                                    ' --tilesize=' + tilesize +
                                    ' --interpnumngb=1 ' +
                                    ' --qthresh=' + qthresh +
                                    ' --minnumfalse=1' +
                                    ' --config=' + noisechisel_cfg_file)
                os.system(line_noisechisel)

                # Segment
                line_segment = ('astsegment ' + imaux_detected +
                                ' --output=' + imaux_segmented +
                                ' --tilesize=' + tilesize +
                                ' --interpnumngb=1 ' +
                                ' --minnumfalse=1' +
                                ' --config=' + segment_cfg_file)
                os.system(line_segment)

                # Catalogue
                line_catalogue = ('astmkcatalog ' + imaux_segmented +
                                  ' --output=' + imaux_catalogue +
                                  ' --config=' + mkcatalogue_cfg_file +
                                  ' --objid --x --y')
                os.system(line_catalogue)

            except:
                rmline = 'rm ' + image_fits + ' ' + image_swarp + \
                         ' ' + im_segmented + ' ' + im_detected + \
                         ' ' + imaux_detected + ' ' + imaux_segmented + \
                         ' ' + imaux_catalogue
                os.system(rmline)
                n = n + 1
                continue

            # Get pixel object value corresponding to x, y from USNO-B1
            objids = fits.getdata(imaux_segmented, 'OBJECTS')
            cvalue = objids[int(y), int(x)]

            # Read the x y values for this object from the computed catalogue
            dcatalogue = fits.getdata(imaux_catalogue, 'OBJECTS')
            ixy = dcatalogue[cvalue - 1]

            # Because of catatalogue start counting in 1, subtract -1 to pixels
            # These x y values are the center that `swarp' will use
            x = ixy[1] - 1
            y = ixy[2] - 1

        # Run Imfit to obtain the center
        if True:

            imfit_out = rfa.PyImfit_Moffat((image_fits, 0), (x, y), (31, 31), deltemp=True)

            x = imfit_out['x_f'] - 1
            y = imfit_out['y_f'] - 1


        print('')
        print('CENTERING with SWarp')
        print('')

        # Define necessary images
        image_swarp = image_fits + '_swarp.fits'
        image_weight = image_fits + '_weight.fits'

        swarp_line = ('swarp ' + image_fits + '[0]' +
                      ' -c '+ swarp_configfile +
                      ' -IMAGEOUT_NAME ' + image_swarp +
                      ' -CENTER ' + str(x) + ',' + str(y) +
                      ' -IMAGE_SIZE ' + str(dimpix) + ',' + str(dimpix) +
                      ' -RESAMPLE_DIR ' + path_out +
                      ' -WEIGHTOUT_NAME ' + image_weight)

        # Run Swarp and put nans where SWarp has filled with zeros
        os.system(swarp_line)

        data_swarped = fits.getdata(image_swarp, 0)
        header_swarped = fits.getheader(image_swarp, 0)

        data_swarped[data_swarped == 0] = np.nan

        # Save centered and masked outer part with nans image
        hdu_out = fits.PrimaryHDU(data_swarped.astype(np.float32))
        hdu_out.header = header_swarped
        hdu_out.verify('fix')

        hdu_out.writeto(image_swarp, overwrite=True)

    if False:
        # Once the star has been recentered, auxiliar images are not necessary
        rmline = 'rm ' + imaux_detected + \
                 ' '   + imaux_segmented + \
                 ' '   + imaux_catalogue

        os.system(rmline)


    """ AT THIS POINT, THE STAR HAS BEEN RECENTERED"""

    # Obtaining detection map with `astnoisechisel'
    print('')
    print('NOISECHISEL')
    print('')
    im_detected = image_swarp + '_detected.fits'
    rmlist.append(im_detected)

    try:
        # Noisechisel parameters
        tilesize = '100,100'
        qthresh = '0.5'
        line_noisechisel = ('astnoisechisel --hdu=0 ' + image_swarp +
                            ' --output=' + im_detected +
                            ' --tilesize=' + tilesize +
                            ' --interpnumngb=1 ' +
                            ' --qthresh=' + qthresh +
                            ' --minnumfalse=1' +
                            ' --config=' + noisechisel_cfg_file)
        # Run noisechisel
        os.system(line_noisechisel)
        rmline = 'rm ' + image_weight
        os.system(rmline)

    except:
        # If noisechisel has failed, then remove the images and continue
        # with the following star.
        rmline = 'rm ' + image_fits + ' ' + image_swarp + \
                 ' ' + im_segmented + ' ' + im_detected
        os.system(rmline)
        n = n + 1
        continue


    # Obtaining object masks with `astsegment'
    print('')
    print('ASTSEGMENT')
    print('')
    im_segmented = im_detected + '_label.fits'
    rmlist.append(im_segmented)

    try:
        # Segment parameters
        line_segment = ('astsegment ' + im_detected +
                        ' --output=' + im_segmented +
                        ' --tilesize=' + tilesize +
                        ' --interpnumngb=1 ' +
                        ' --minnumfalse=1' +
                        ' --config=' + segment_cfg_file)
        # Run segment
        os.system(line_segment)

    except:
        # If segment has failed, then remove the images and continue
        # with the following star.
        rmline = 'rm ' + image_fits + ' ' + image_swarp + \
                 ' ' + im_segmented + ' ' + im_detected
        os.system(rmline)
        n = n + 1
        continue

    # One more check, if any of the two images have been created, then
    # continue with the following star
    if os.path.isfile(im_detected) == False or os.path.isfile(im_segmented) == False:
        rmline = 'rm ' + image_fits + ' ' + image_swarp + \
                 ' ' + im_detected + ' ' + im_segmented
        os.system(rmline)
        n = n + 1
        continue


    # Mask objects using segmentation map
    print('')
    print('MASKING')
    print('')

    image_masked1 = image_swarp + '_mobjects.fits'

    objects = fits.getdata(im_segmented, 'OBJECTS')
    objects[objects==objects[objects.shape[0]//2,objects.shape[1]//2]] = 0
    objects[objects != 0] = 1

    data_swarp = fits.getdata(image_swarp, 0)
    data_swarp[objects == 1] = np.nan
    hdu = fits.PrimaryHDU(data_swarp.astype(np.float32))
    hdu.writeto(image_masked1, overwrite=True)

    # Bleeding rejection
    if reject == True:
        """ REJECTING BAD IMAGES """
        print('')
        print('REJECTING BLEED SATURATED STAR')
        print('')

        dat = fits.getdata(image_masked1, 0)

        dat[y1_mask:y2_mask,:] = np.nan

        p1 = np.nanmedian(dat, axis=0)
        left = p1[x1_left:x2_left]
        left_min = np.nanmin(left)

        center = p1[x1_center:x2_center]
        center_min = np.nanmin(center)

        right = p1[x1_right:x2_right]
        right_min = np.nanmin(right)

        fa = np.nanmin((left_min, right_min)) / center_min

        vstd = np.nanstd(p1)
        vmin = np.nanmin(p1)

        f = np.abs(vmin / vstd)

        if (fa > 1) or (fa <= 0) or (f > 1.5):

            print('REJECTED!')
            # DELETE images
            rmline = 'rm ' + image_fits
            os.system(rmline)

            rmline = 'rm ' + image_swarp
            os.system(rmline)

            rmline = 'rm ' + im_segmented
            os.system(rmline)

            rmline = 'rm ' + im_detected
            os.system(rmline)

            rmline = 'rm ' + image_masked1
            os.system(rmline)

            rmline = 'rm ' + im_normalized
            os.system(rmline)

            rmline = 'rm ' + image_weight
            os.system(rmline)

            n = n + 1
            continue


    # Normalization
    print('')
    print('NORMALIZATION')
    print('')

    dat_masked = fits.getdata(image_masked1, 0)

    im_normalized = image_swarp + f'_{mag:.2f}_normalized1.fits'

    profile = rfa.radial_profile(dat_masked, pixelscale, \
              function='sigma_clip', rboundaries=(0, rnormpix+10),\
              sigma_cut=3.0, center=None, symetrize=None)

    profile = profile['radial_profile']

    vnorm = profile['smedian'][rnormpix]
    """ Delete images if vnorm value is NAN """
    if np.isnan(vnorm):
        rmline = 'rm ' + image_fits
        os.system(rmline)

        rmline = 'rm ' + image_swarp
        os.system(rmline)

        rmline = 'rm ' + im_segmented
        os.system(rmline)

        rmline = 'rm ' + im_detected
        os.system(rmline)

        rmline = 'rm ' + image_masked1
        os.system(rmline)

        rmline = 'rm ' + image_weight
        os.system(rmline)

        n = n + 1
        continue

    dat_norm = dat_masked / vnorm

    # Save centered, masked and normalized star
    if True:
        """ Read original data and save new data (normalized and masked image) """
        hdu_swarp = fits.open(image_swarp)

        # SWARP IMAGE
        hdu_swarp[0].header['RA_USNO'] = ra
        hdu_swarp[0].header['DEC_USNO'] = de
        hdu_swarp[0].header['MAG_USNO'] = mag
        hdu_swarp[0].header['VNORM'] = vnorm
        hdu_swarp.verify('fix')
        hdu_swarp.writeto(image_swarp, overwrite=True)


        # NORMALIZED AND MASKED IMAGE
        hdu_norm = fits.PrimaryHDU(dat_norm.astype(np.float32))
        hdu_norm.header['VNORM'] = vnorm
        hdu_norm.name = 'IMAGE'
        hdu_norm.verify('fix')


        # PROFILE
        tprofile = Table(profile)
        hdu_prof = fits.BinTableHDU(tprofile)
        hdu_prof.verify('fix')
        hdu_prof.name = 'PROFILE'

        # SAVE FINAL DATA: NORMALIZED AND MASKED IMAGE WITH PROFILE
        new_hdul = fits.HDUList([hdu_norm, hdu_prof])
        new_hdul.writeto(im_normalized, overwrite=True)

        l_ra.append(ra)
        l_de.append(de)
        l_mag.append(mag)
        l_vnorm.append(vnorm)
        l_rnormpix.append(rnormpix)
        l_lpath.append(len(path_out)+1)
        l_image_swarp.append(image_swarp)
        l_rnormarcsec.append(rnormarcsec)
        l_logvnorm.append(np.log10(vnorm))
        l_im_normalized.append(im_normalized)

        # DELETE images
        rmline = 'rm ' + image_fits
        os.system(rmline)

        rmline = 'rm ' + im_segmented
        os.system(rmline)

        rmline = 'rm ' + im_detected
        os.system(rmline)

        rmline = 'rm ' + image_masked1
        os.system(rmline)

        rmline = 'rm ' + image_weight
        #os.system(rmline)

        n_good = n_good + 1
        n = n + 1

# Create statistics table columns
c_image_swarp = Column(l_image_swarp, name='im_swarp')
c_im_normalized1 = Column(l_im_normalized, name='im_normalized1')
c_ra = Column(l_ra, name='ra')
c_de = Column(l_de, name='dec')
c_mag = Column(l_mag, name='mag')
c_vnorm1 = Column(l_vnorm, name='vnorm1')
c_logvnorm1 = Column(l_logvnorm, name='logvnorm1')
c_rnormarcsec = Column(l_rnormarcsec, name='rnormarcsec')
c_rnormpix = Column(l_rnormpix, name='rnormpix')
c_lpath = Column(l_lpath, name='lpath')


# Inject table columns
stats.add_column(c_image_swarp)
stats.add_column(c_im_normalized1)
stats.add_column(c_ra)
stats.add_column(c_de)
stats.add_column(c_mag)
stats.add_column(c_vnorm1)
stats.add_column(c_logvnorm1)
stats.add_column(c_rnormarcsec)
stats.add_column(c_rnormpix)
stats.add_column(c_lpath)

# Save statistics table
stats.write(output_file, format='fits', overwrite=True)

