# Rejecting inner stars by radial profile analysis
#
# This Python script plots the profile of the stacked PSF in the interation
# 2, the radial profile of a particular star, and the radial profile of an
# object that it is not a star but a galaxy. It is clearly seen that the
# radial profiles are different and that is the reason for rejecting the non
# stars objects for the final combination.
#
# Original author:
#     Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s):
# Copyright (C) 2019, Raul Infante-Sainz.
#
# This Python script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Python script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.






# Import modules
import sys
import numpy as np
from astropy.io import fits

# Matplotlib crashes in Mac OS systems
# To avoid this crash, use the condition:
from sys import platform as sys_pf
if sys_pf == 'darwin':
    import matplotlib
    matplotlib.use("TkAgg")

import matplotlib.pyplot as plt
plt.style.use('classic')




# Read input arguments
psf      =  sys.argv[1]
star     =  sys.argv[2]
galaxy   =  sys.argv[3]
plot_out =  sys.argv[4]


# Set necessary parameters
pixelscale = 0.396 # pixel scale
rnormarcsec = 2    # arcsec
rnormpix = int(rnormarcsec / pixelscale)


r_i = 2    # arcsec
r_o = 10  # arcsec


r_i = int(r_i/pixelscale)
r_o = int(r_o/pixelscale)


# Check region
dpix = 1
dpixx = 3

ppsf    = fits.getdata(psf,1)
pstar   = fits.getdata(star,1)
pgalaxy = fits.getdata(galaxy,1)
XMAX    = np.nanmax(ppsf['radii_scale'])

w1 = np.nanmedian(ppsf['smean'][r_i-dpix:r_i+dpix])
w2 = np.nanmedian(ppsf['smean'][r_o-dpix:r_o+dpix])


# Normalize profiles
ppsf['smean'] = ppsf['smean'] / ppsf['smean'][rnormpix]
pstar['smean'] = pstar['smean'] / pstar['smean'][rnormpix]
pgalaxy['smean'] = pgalaxy['smean'] / pgalaxy['smean'][rnormpix]

c1 = np.nanmedian(pstar['smean'][r_i-dpix:r_i+dpix])
c2 = np.nanmedian(pstar['smean'][r_o-dpix:r_o+dpix])


# Data to be saved
comparation = []


# STAR
star_ratio = pstar['smean'][r_i-dpixx:r_i+dpixx] / ppsf['smean'][r_i-dpixx:r_i+dpixx]
star_vmean = np.ma.mean(star_ratio)
star_vstd = np.ma.std(star_ratio)
comparation.append(['STAR',star_vmean, star_vstd])
print("STAR", star_vmean, star_vstd)


# GALAXY
galaxy_ratio = pgalaxy['smean'][r_i-dpixx:r_i+dpixx] / ppsf['smean'][r_i-dpixx:r_i+dpixx]
galaxy_vmean = np.ma.mean(galaxy_ratio)
galaxy_vstd = np.ma.std(galaxy_ratio)
comparation.append(['GALAXY',galaxy_vmean, galaxy_vstd])
print("GALAXY", galaxy_vmean, galaxy_vstd)


# Criteria
# if (star_vmean < 0.5) or (star_vmean > 1.5) or (star_vstd >= 0.2):

fig = plt.figure()
ax = fig.add_subplot(111)

# Plot the profiles
ax.plot(ppsf['radii_scale'], ppsf['smean'], \
         color='k', label='psf1', linewidth=2, alpha=0.8)

ax.plot(pstar['radii_scale'], pstar['smean'], \
         linestyle='dashed', color='b', label='Star', linewidth=3, alpha=0.8)

ax.plot(pgalaxy['radii_scale'], pgalaxy['smean'], \
         linestyle='dashdot', color='r', label='Galaxy', linewidth=3, alpha=0.8)

# Plot vertical check region
ax.axvspan(ppsf['radii_scale'][r_i-dpixx], ppsf['radii_scale'][r_i+dpixx], \
            facecolor='k', alpha=0.3, hatch='//', label='Control region')


# Plot parameters
ax.set_xlim(1e-1, 100)
ax.set_ylim(1e-4, 200)
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_ylabel('Normalized flux', fontsize=16)
ax.set_xlabel('Distance from centre [arcsec]', fontsize=16)
#plt.grid()
axticks = [0.1, 1, 10, 100]
axticklabels = [0.1, 1, 10, 100]

ax.set_xticks(axticks)
ax.set_xticklabels(axticklabels)
ax.legend()


# Saving figure
plt.savefig(plot_out, format='pdf', bbox_inches='tight')
print(plot_out, 'SAVED! ')


# Save .txt file with data
statistics_outfile = plot_out.replace('.pdf','.dat')
np.savetxt(statistics_outfile, comparation, fmt="%s", \
           header="OBJECT, MEAN, S")
