# Build the outer part of the PSF
#
# This Makefile is written to construct the outer part of the PSF. It takes
# a catalogue of very bright stars previously constructed by matching very
# bright stars and SDSS central position of images. As a consequence, this
# catalogue contains all necessary data of the very bright stars, SDSS
# images in which they are, and the shell command to download the image.
#
# Iteration 1 consists in going star by star doing the following:
#   - Create a new image with the center in the center of the star.
#   - Obtain object detection map and mask the image.
#   - Compute the radial profile and normalize the image at given radius.
#   - Once these steps have been done for all stars, save a table with
#   global statistics.
#   -Finally, combine all masked and normalized images to obtain the PSF.
#
# Iteration 2 consists in subtract the obtained PSF and run the same
# process. Now the masks will be improved because the bright star has been
# removed and there is not so strong gradient of light in the image:
#   - Subtract the PSF iteration 1 to each original image.
#   - Obtain mask objects and mask the image.
#   - Normalize the images using radial profiles at some radius.
#   - Once these steps have been done for all stars, save a table with
#   global statistics.
#   - Finally, combine all masked and normalized images to obtain the PSF.
#
# Iteration 3 consists in mask the outer part of the stars according to the
# sky brightness. The idea is to mask the outer region of each stars where
# the sky starts to dominate. To do that, the radius at which the radial
# profile reach the sky level is computed, then the relation radius vs flux
# is fitted and this function is used to mask the outer region of the stars
# according to the brightness.
#
# Finally, the images prepared following all the previous steps are combined
# to obtain the outer part of the PSF.
#
# Original author:
#     Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s):
# Copyright (C) 2019, Raul Infante-Sainz.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# Directories
# -----------
#
bouter-outdir = $(BDIR)/outer-part


# Build the outer part of the PSF: iteration 1
# --------------------------------------------
#
# First iteration of the building of the PSF consists in download the stars
# and prepare them to be stacked. The preparation consists in recenter each
# star into a new image with the RA and DEC given by USNO-B1 catalogue. Once
# it has been done, objects are masked and the image is normalized to a
# given radius. The result are a lot of images ready to be combined an also
# a .fits table with all statistics for the following steps.
stars-catalogue-outer = $(INDIR)/stars-catalogue-outer.fits
pyscript-it1-outer = $(pythondir)/build-stamps-iteration-1.py
stat-it1-files-outer = $(foreach b,$(BANDS),$(bouter-outdir)/stats-iteration-1-filter-$(b).fits)
$(bouter-outdir): ; mkdir $@
$(stat-it1-files-outer): $(bouter-outdir)/stats-iteration-1-filter-%.fits: \
                         $(pyconfigdir)/outer-parameters.yaml \
                         | $(bouter-outdir)
	python $(pyscript-it1-outer) \
	       --band=$* \
	       --nmax=$(NSTARS) \
	       --output_file=$@ \
	       --path_out=$(bouter-outdir) \
	       --swarp_cf=$(swarp-cfgfile) \
	       --segment_cf=$(astsegment-cfgfile) \
	       --starcat=$(stars-catalogue-outer) \
	       --noisechisel_cf=$(astnoisechisel-cfgfile) \
	       --yaml_cf=$(pyconfigdir)/outer-parameters.yaml




# Stack stamps: iteration 1
# -------------------------
#
# Stack all images saved in the stat files. They are already centered,
# masked and normalized.
psfs-it1-outer = $(foreach b,$(BANDS),$(bouter-outdir)/psf-iteration-1-filter-$(b).fits)
$(psfs-it1-outer): $(bouter-outdir)/psf-%.fits: $(bouter-outdir)/stats-%.fits
	# Stack all images together
	astarithmetic $$(asttable $< -cim_normalized1) -g0 \
		      $$(echo $$(asttable $< -c1) | wc -w) \
	              $(STACKING-OPERATOR) --output=$@





# Build the outer part of the PSF: iteration 2
# --------------------------------------------
#
# Second iteration of the building PSF. Here the obtained PSF in the
# iteration 1 is subtracted from each original single star. Then, new mask
# and new normalization is obtained. The reason of doing this second
# iteration is that the masking process improves a lot with the removal of
# the bright star. It is possible even to obtain masks for the internal
# reflexions and diffraction spikes. As a consequence, the normalization
# also changes to, in principle, better value.
tilesizes-file = $(INDIR)/random-tilesizes.dat
pyscript-it2-outer = $(pythondir)/build-stamps-iteration-2.py
stat-it2-files-outer = $(foreach b,$(BANDS),$(bouter-outdir)/stats-iteration-2-filter-$(b).fits)
$(stat-it2-files-outer): $(bouter-outdir)/stats-iteration-2-filter-%.fits: \
                         $(bouter-outdir)/stats-iteration-1-filter-%.fits \
                         $(bouter-outdir)/psf-iteration-1-filter-%.fits \
                         $(pyconfigdir)/outer-parameters.yaml \
			 $(tilesizes-file)
	python $(pyscript-it2-outer) \
	       --band=$* \
	       --nmax=$(NSTARS) \
	       --output_file=$@ \
	       --path_out=$(bouter-outdir) \
	       --tilesizesfile=$(tilesizes-file) \
	       --segment_cf=$(astsegment-cfgfile) \
	       --noisechisel_cf=$(astnoisechisel-cfgfile) \
	       --yaml_cf=$(pyconfigdir)/outer-parameters.yaml \
	       --psf1=$(bouter-outdir)/psf-iteration-1-filter-$*.fits \
	       --starcat=$(bouter-outdir)/stats-iteration-1-filter-$*.fits



# Stack stamps: iteration 2
# -------------------------
#
# Stack all images saved in the stat files. They are already centered,
# masked and normalized.
psfs-it2-outer = $(foreach b,$(BANDS),$(bouter-outdir)/psf-iteration-2-filter-$(b).fits)
$(psfs-it2-outer): $(bouter-outdir)/psf-%.fits: $(bouter-outdir)/stats-%.fits
	# Stack all images together
	astarithmetic $$(asttable $< -cim_normalized2) -g0 \
		      $$(echo $$(asttable $< -c1) | wc -w) \
	              $(STACKING-OPERATOR) --output=$@





# Masking outer part of stars: iteration 3
# ----------------------------------------
#
# This step is the masking of the outer part of the PSF according to the
# bright of the star and the radius at which the stars reach the sky
# background level.
pyscript-it3-outer = $(pythondir)/build-stamps-iteration-3.py
stat-it3-files-outer = $(foreach b,$(BANDS),$(bouter-outdir)/stats-iteration-3-filter-$(b).fits)
$(stat-it3-files-outer): $(bouter-outdir)/stats-iteration-3-filter-%.fits: \
                         $(bouter-outdir)/stats-iteration-2-filter-%.fits \
                         $(pyconfigdir)/outer-parameters.yaml
	python $(pyscript-it3-outer) \
	       --band=$* \
	       --output_file=$@ \
	       --yaml_cf=$(pyconfigdir)/outer-parameters.yaml \
	       --starcat=$(bouter-outdir)/stats-iteration-2-filter-$*.fits \
	       --plot_file=$(bouter-outdir)/stats-iteration-3-filter-$*-plot.pdf \
	       --table_all=$(bouter-outdir)/stats-iteration-3-filter-$*-all.fits \
	       --table_filtered=$(bouter-outdir)/stats-iteration-3-filter-$*-filtered.fits



# Stack stamps: iteration 3
# -------------------------
#
# Stack all images saved in the stat files. They are already centered,
# masked and normalized.
psfs-it3-outer = $(foreach b,$(BANDS),$(bouter-outdir)/psf-iteration-3-filter-$(b).fits)
$(psfs-it3-outer): $(bouter-outdir)/psf-%.fits: $(bouter-outdir)/stats-%.fits
	# Stack all images together
	astarithmetic $$(asttable $< -cim_tocombine) -g0 \
		      $$(echo $$(asttable $< -c1) | wc -w) \
	              $(STACKING-OPERATOR) --output=$@





# Final TeX macro
# ---------------
#
# Make an empty .tex file as final file when the outer part of the PSF has
# been generated.
$(mtexdir)/build-outer.tex: $(psfs-it2-outer) $(psfs-it3-outer) | $(mtexdir)
	echo "%empty" > $@

