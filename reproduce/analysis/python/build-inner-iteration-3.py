## Modules
import numpy as np
from astropy.io import fits
from astropy import wcs
from astropy.table import Table,Column
from astropy.stats import sigma_clip
import glob
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
plt.style.use('classic')
import yaml
import sys
import os
plt.ioff()

configfile = sys.argv[1]
im_psf = sys.argv[2]
table_name_input = sys.argv[3]
table_name_all   = sys.argv[4]
table_name_output = sys.argv[5]

#table_name_output_filtered = sys.argv[4]
#plot_name = sys.argv[5]
#table_name_output_tocombine = sys.argv[6]

print('++++++++++++++++++++++++++++++++++')
print('                                  ')
print(sys.argv)
print('configuration file :: ', configfile)
print('im_psf             :: ', im_psf)
print('table_name_input   :: ', table_name_input)
print('table_name_output  :: ', table_name_output)

print('                                  ')
print('++++++++++++++++++++++++++++++++++')
with open(configfile, 'r') as cfg_file:
    """ PARAMETERS """
    cfg = yaml.load(cfg_file)
    pg = cfg['GENERAL']
    pl = cfg['psf5_inner_it3_filtering_profiles.py']

    ## PATHS AND MODULES
    path_module = pg['path_module']
    #path_out = pg['path_out']

    sys.path.append(path_module)
    import raulisfpy.rfastro as rfa
    import raulisfpy.rfgeneral as rfg
    sys.path.remove(path_module)
    ##

    band = pg['band']
    rnormarcsec = pg['rnormarcsec']
    pixelscale = pg['pixelscale']
    nmax = pg['nmax']
    mag_kw = pg['mag_kw']
    dimpix = pg['dimpix']

    label =pg['label']
    #table_name_input = path_out + '/' + cfg['psf3_inner_it2_stamp.py']['table_name_output']
    #table_name_output = path_out + '/' + pl['table_name_output_tocombine']
    #table_name_all = path_out + '/' + pl['table_name_output_all']
    #im_psf = path_out + '/' + 'psf_' + band + '_inner_it2.fits'
    im_psf_profile = im_psf + '_profile.fits'

table_input = fits.getdata(table_name_input, 1)
table_aux = Table(table_input)

# Make the radial profile of the PSF
if True:
    data = fits.getdata(im_psf, 1)

    profile = rfa.radial_profile(data, pixelscale, function='sigma_clip', rboundaries=(0, 'Rmax'), sigma_cut=3.0, center=None, symetrize=None, save_profile=im_psf_profile)
    profile = profile['radial_profile']

rnormpix = int(rnormarcsec / pixelscale)
r_i = 2    # arcsec
r_o = 10  # arcsec

r_i = int(r_i/pixelscale)
r_o = int(r_o/pixelscale)

dpix = 1
dpixx = 3


with fits.open(im_psf_profile) as hdu_psf_profile:
    p_psf = hdu_psf_profile[1].data
    w1 = np.nanmedian(p_psf['smean'][r_i-dpix:r_i+dpix])
    w2 = np.nanmedian(p_psf['smean'][r_o-dpix:r_o+dpix])




l_s = []
for row in table_input:
    im = row['im_normalized2']
    profile = fits.getdata(im, 'PROFILE')

    p_star = profile['smean'] / profile['smean'][rnormpix]

    ratio = p_star[r_i-dpixx:r_i+dpixx] / p_psf['smean'][r_i-dpixx:r_i+dpixx]
    fdata = ratio#sigma_clip(ratio)
    vmean = np.ma.mean(fdata)
    vstd = np.ma.std(fdata)

    l_s.append(vstd)

c_s = Column(l_s, name='s')

table_aux.add_column(c_s)
table_aux.write(table_name_all, format='fits', overwrite=True)

s_filtered = sigma_clip(l_s)
s_maxgood = np.max(s_filtered.data[~s_filtered.mask])
table_filtered = table_aux.copy()
n = 0
for row in table_filtered:
    if row['s'] > s_maxgood:
        print('REJECTED',row['im_normalized2'])
        table_filtered.remove_row(n)
    else:
        print(' ACEPTED',row['im_normalized2'])
    n = n + 1


table_filtered.add_column(Column(table_filtered['im_normalized2'], name='im_tocombine'))
table_filtered.write(table_name_output, format='fits', overwrite=True)

