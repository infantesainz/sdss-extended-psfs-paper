# Plot of profiles along drift scanning direction and perpendicular
#
# This Python script makes the plot comparing the flux along the drift
# scanning direction and flux along the perpendicular direction of the drift
# scanning direction.
#
# Original author:
#     Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s):
# Copyright (C) 2019, Raul Infante-Sainz.
#
# This Python script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Python script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# Import modules
import sys
import numpy as np
from astropy.io import fits

# Matplotlib crashes in Mac OS systems
# To avoid this crash, use the condition:
from sys import platform as sys_pf
if sys_pf == 'darwin':
    import matplotlib
    matplotlib.use("TkAgg")
import matplotlib.pylab as plt


# Read input arguments
psfims_aux    = sys.argv[1]    # All PSF images
output_plot   = sys.argv[-1]   # Target, name of the final plot

# First argument is all PSF images separated by a space, it is necessary to
# construct a list from this string. To do that, use `.split()'.
limpsf = psfims_aux.split()



pixelscale = 0.396

plt.clf()
plt.ion()
fig = plt.figure()
ax1 = fig.add_subplot(111)
ax2 = ax1.twiny()
bands = ['u', 'g', 'r', 'i', 'z']
colors  = ['tab:blue', 'tab:green', 'tab:red', 'tab:orange', 'tab:purple']
factors = [1e7, 1e5, 1e3, 1e1, 1e-1]


w = 51
alpha = 0.8
n = 0
ymins = []
ymaxs = []
comparation = []
for impsf in limpsf:
    data = fits.getdata(impsf, 1)
    shape = data.shape
    cy, cx = int(shape[0]/2), int(shape[1]/2)

    d_drif = data[cy-w:cy+w, :]
    dd_drif = np.nanmedian(d_drif, axis=0)
    dt_drif = np.flip(dd_drif[:cx+1], axis=0) + dd_drif[cx:]

    d_perp = data[:, cx-w:cx+w]
    dd_perp = np.nanmedian(d_perp, axis=-1)
    dt_perp = np.flip(dd_perp[:cx+1], axis=0) + dd_perp[cx:]
    plt.ion()

    #plt.plot(100*(dd_drif-dd_perp)/dd_drif, label=impsf, alpha=alpha)
    #plt.plot(dt_perp, 'r-', label='perp', alpha=alpha)
    #plt.plot(dt_drif, 'b-', label='drif', alpha=alpha)


    y_p = dd_perp
    y_d = dd_drif
    x = np.arange(-len(dd_drif)//2, len(dd_drif)//2)

    ax1.plot(x, y_p/np.nanmax(y_p)*factors[n], '-', color=colors[n], alpha=alpha)
    ax1.plot(x, y_d/np.nanmax(y_d)*factors[n], ':', color=colors[n], alpha=alpha)
    ytext = np.mean((np.nanmin(y_p/np.nanmax(y_p)*factors[n]), np.nanmin(y_d/np.nanmax(y_d)*factors[n])))
    xtext = 500/pixelscale

    plt.text(xtext, ytext, bands[n], color=colors[n], fontsize=18, \
             verticalalignment='center', horizontalalignment='center')
    ymins.append(np.nanmin(y_p/np.nanmax(y_p)*factors[n]))
    ymaxs.append(np.nanmax(y_p/np.nanmax(y_p)*factors[n]))

    ratio = y_d / y_p
    median_ratio = np.nanmedian(ratio)
    std_ratio = np.nanstd(ratio)
    print(impsf, 'F drift / F perp', median_ratio, 'std', std_ratio)
    comparation.append([impsf, median_ratio, std_ratio])

    n = n + 1

# Transform ratio between F drift scanning direction and F prerpendicular
comparation = np.array(comparation)

# Save .txt file with data
statistics_outfile = output_plot.replace('.pdf','.dat')
np.savetxt(statistics_outfile, comparation, fmt="%s", \
           header="IMAGEPSF, MEDIANRATIO, STDRATIO")


xarcsec = np.arange(-500, 500+100, 100) #x*pixelscale
xarcsec_label = [str(i) for i in xarcsec]
x_arcsec_pix = xarcsec/pixelscale
ax1.set_xticks(x_arcsec_pix)
ax1.set_xticklabels(xarcsec_label)
ax1.set_xlabel('Distance from centre [arcsec]', fontsize=16)

ax2.set_xlim(ax1.get_xlim())
xarcmin = np.arange(-8, +8+1, 2)
xarcmin_label = [str(i) for i in xarcmin]
x_arcmin_pix = xarcmin/pixelscale*60
ax2.set_xticks(x_arcmin_pix)
ax2.set_xticklabels(xarcmin_label)
ax2.set_xlabel('Distance from centre [arcmin]', fontsize=16)
#ax2.grid(True)

ymin, ymax = ax2.get_ylim()
plt.ylim(0.3*np.nanmin(ymins), 3*np.nanmax(ymaxs))
plt.yscale('log')
plt.savefig(output_plot, format='pdf', bbox_inches='tight')
print(output_plot, ' SAVED!')

