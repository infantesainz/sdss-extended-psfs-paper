
import numpy as np
from astropy.io import fits
from astropy.wcs import WCS
import sys
import glob
import os
import yaml

# Matplotlib crashes in Mac OS systems
# To avoid this crash, use the condition:
from sys import platform as sys_pf
if sys_pf == 'darwin':
    import matplotlib
    matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
plt.style.use('classic')



configfile = sys.argv[1]
im_psf_wing= sys.argv[2]
im_psf_aureole = sys.argv[3]
im_psf_core= sys.argv[4]
crop_cfgfile = sys.argv[5]
path_out     = sys.argv[6]
psf_im_out   = sys.argv[7]

print('++++++++++++++++++++++++++++++++++')
print('                                  ')
print(sys.argv)
print('                                  ')
print('++++++++++++++++++++++++++++++++++')




with open(configfile, 'r') as cfg_file:
    """ INPUT PARAMETERS """

    cfg = yaml.load(cfg_file)
    pg = cfg


    ## PATHS AND MODULES
    path_module = pg['path_module']
    #path_out = pg['path_out']
    path_in = pg['path_in']
    cwd = os.getcwd()

    sys.path.append(path_module)
    #import sdss_psf_pyfunctions as rfa
    import raulisfpy.rfastro as rfa
    import raulisfpy.rfgeneral as rfg
    sys.path.remove(path_module)
    ##

    filt = pg['band']
    pixscale = pg['pixelscale']
    plot_snr = pg['plot_snr']
    plot_legends = pg['plot_legends']
    yax_min = 1e-4 #pg['yax_min']
    yax_max = 1e+7 #pg['yax_max']

    #rtot = 8.0    # in arcmin
    Rtot = pg['Rtot']  #1212# int(rtot*60.0/0.396)


    ## Region in --->>>>>> ARCSEC <<<<<<----- where the AUREOLE-WING junction will be done
    rmin_w = pg['rmin_w']
    rmax_w = pg['rmax_w']
    w_a = pg['w_a']

    ## Region in --->>>>>> ARCSEC <<<<<<----- where the CORE-AUREOLE-WING junction will be done
    rmin_c = pg['rmin_c']
    rmax_c = pg['rmax_c']
    w_c = pg['w_c']

    ## Original names of the psf images
    #im_psf_core = path_in + '/' +  pg['im_psf_inner']
    #im_psf_aureole = path_in + '/' + pg['im_psf_inter']
    #im_psf_wing = path_in + '/' + pg['im_psf_outer']

    ## Name of the aureole-wing image
    im_psf_junction = path_out + '/' + pg['im_psf_junction']

    ## Name of the total psf image
    im_psf_total = path_out + '/' + pg['im_psf_total']
    im_psf_finalplot = path_out + '/' + pg['im_psf_finalplot']
"""
## PSF WING NORMALIZATION
wing_dat = fits.open(im_psf_wing)[0].data    # HDU list of the outer part of the psf
wing_dat = wing_dat/np.nansum(wing_dat)
hdu_out = fits.PrimaryHDU(wing_dat.astype(np.float32))    # Create HDU file
hdu_out.writeto(im_psf_wing, overwrite=True)    # Save .fits

## PSF AUREOLE NORMALIZATION
aureole_dat = fits.open(im_psf_aureole)[0].data    # HDU list of the outer part of the psf
aureole_dat = aureole_dat/np.nansum(aureole_dat)
hdu_out = fits.PrimaryHDU(aureole_dat.astype(np.float32))    # Create HDU file
hdu_out.writeto(im_psf_aureole, overwrite=True)    # Save .fits

## PSF CORE NORMALIZATION
core_dat = fits.open(im_psf_core)[0].data    # HDU list of the outer part of the psf
core_dat = core_dat/np.nansum(core_dat)
hdu_out = fits.PrimaryHDU(core_dat.astype(np.float32))    # Create HDU file
hdu_out.writeto(im_psf_core, overwrite=True)    # Save .fits
"""
a = 0.7

legend_profiles = []
legend_regions = []
## JUNCTION INTERMEDIATE-OUTER
if True:
    psf_inner = (im_psf_aureole, 1)
    psf_outer = (im_psf_wing, 1)
    psf_out = im_psf_junction
    rmin = rmin_w
    rmax = rmax_w
    zone = 'AJ'
    w = w_a

    psf_inner_big = psf_inner[0].replace('.fits', '_big.fits')

    ## PSF-OUTER PART
    hdu_outer = fits.open(psf_outer[0])    # HDU list of the outer part of the psf
    dat_outer = hdu_outer[psf_outer[1]].data    # Get data of the outer part of the psf
    hdr_outer = hdu_outer[psf_outer[1]].header

    #center_outer = rf.center_position(dat_outer, precission='subpixel')    # Center of the outer psf-array-dat
    shape_outer = np.shape(dat_outer)    # Shape of the outer psf-array-dat
    center_outer = rfa.center_position(shape_outer, precission='subpixel', dtype='int')


    dat_outer_copy = dat_outer.copy()    # Copy of the psf-outer array dat


    ## PSF-INNER PART
    hdu_inner = fits.open(psf_inner[0])    # HDU list of the inner part of the psf
    dat_inner = hdu_inner[psf_inner[1]].data    # Get data of the inner part of the psf


    #center_inner = rf.center_position(dat_inner.shape, precission='subpixel')    # Center of the inner psf-array-dat
    shape_inner = np.shape(dat_inner)    # Shape of the inner psf-array-dat
    center_inner = rfa.center_position(shape_inner, precission='subpixel', dtype='int')
    print("center_inner", center_inner)

    ###############################
    ###############################

    if os.path.isfile(psf_inner_big) == False:
        ## Execute PySwarp to built the stamp
        #PySwarpOut = rfa.PySwarp((psf_inner[0], 1), configfile='default.swarp', params=PySwarpDic)
        crop_line = ("astcrop " + psf_inner[0] +  " --hdu=1" +
                     " --mode=img --config=" + crop_cfgfile +
                     " --center=" + str(center_inner[0]+1) + "," + str(center_inner[1]+1) +
                     " --width=" + str(shape_outer[0]) + "," + str(shape_outer[1]) +
                     " --output=" + psf_inner_big)

        print(crop_line)
        os.system(crop_line)

    ###############################
    ###############################

    ## PSF-INNER PART (BIGGER)
    hdu_inner = fits.open(psf_inner_big)
    dat_inner = hdu_inner[1].data
    #dat_inner[dat_inner == 0] = np.nan
    dat_inner_copy = dat_inner.copy()

    psf_inner_big_profile = psf_inner_big.replace('.fits', '_profile.fits')
    if os.path.isfile(psf_inner_big_profile) == False:
        pinner = rfa.radial_profile(dat_inner, pixscale, function='sigma_clip', rboundaries=(0, 'Rmax'), sigma_cut=3.0, center=None, symetrize=None, save_profile=psf_inner_big_profile)

    pinner = fits.getdata(psf_inner_big_profile)


    psf_outer_big_profile = psf_outer[0].replace('.fits', '_profile.fits')
    if os.path.isfile(psf_outer_big_profile) == False:
        pouter = rfa.radial_profile(dat_outer, pixscale, function='sigma_clip', rboundaries=(0, 'Rmax'), sigma_cut=3.0, center=None, symetrize=None, save_profile=psf_outer_big_profile)

    pouter = fits.getdata(psf_outer_big_profile)

    ### Position in pixels of the radial profiles
    rmin_pix = np.max(np.where(pinner['radii_scale'] < rmin)[0])
    rmax_pix = np.min(np.where(pouter['radii_scale'] > rmax)[0])


    ## PROFILE SLOPES ('g=gradient')
    ginner = np.gradient(pinner['smean'])
    gouter = np.gradient(pouter['smean'])

    ## Determine which has the minimun range
    linner = len(ginner)
    louter = len(gouter)
    lmin = np.min((linner, louter))

    ## Determine the sign of the gradient
    sign_gradient = ginner[:lmin] * gouter[:lmin]

    ## Determine the difference between the slopes of the PSF and the STAR
    dif_gradient = np.abs(ginner[:lmin] - gouter[:lmin])
    dif_gradient[sign_gradient < 0.0] = np.nan    # Put nans where the difference is so big (different signs)


    ## PROFILES SIGNAL TO NOISE
    dif_sn = np.abs(pinner['ssnr'][:linner] - pouter['ssnr'][:linner])

    ## Calculate the multiplication of this TWO profiles
    ## Explanation: where dif is close to zero, the profiles will be similar in SN and Slope
    dif = dif_sn[:lmin] * dif_gradient[:lmin]    # <<-- Give more importance in difference of SN

    ## Where is the minimun of dif? --> i=radii in pixels where to normalize
    i = np.where(dif[rmin_pix:rmax_pix] == np.nanmin(dif[rmin_pix:rmax_pix]))[0] + rmin_pix


    ##################################################
    ##################################################

    c1 = pinner['smean'][i-w]
    c2 = pinner['smean'][i+w]

    w1 = pouter['smean'][i-w]
    w2 = pouter['smean'][i+w]

    fc = (w2-w1) / (c2-c1)
    c = w1 - c1 * fc
    ##################################################
    ##################################################

    ## Normalization Factor

    #plt.clf()
    #plt.figure()

    fig = plt.figure()
    ax = fig.add_subplot(111)

    # Select log-spaced points
    rrx = pouter['radii_scale'][:Rtot]
    rry = pouter['smean'][:Rtot]


    # Interpolate to have few points
    xi_me = rrx
    yi_me = rry

    xi_me_min = 0.01
    xi_me_max = np.nanmax(xi_me[:-3])
    xn_me = np.geomspace(xi_me_min, xi_me_max, 26)
    yn_me = np.interp(xn_me, xi_me, yi_me)

    lprofile, = plt.plot(xn_me, yn_me, \
                         'b-o', label='Outer', alpha=a, linewidth=1)
    legend_profiles.append(lprofile)

    # Select log-spaced points
    rrx = pinner['radii_scale']
    rry = pinner['smean']

    # Interpolate to have few points
    xi_me = rrx
    yi_me = rry

    xi_me_min = 0.01
    xi_me_max = np.nanmax(xi_me[:-3])
    xn_me = np.geomspace(xi_me_min, xi_me_max, 26)
    yn_me = np.interp(xn_me, xi_me, yi_me)

    lprofile, = ax.plot(xn_me, yn_me, \
                'r--v', label='Intermediate', alpha=a, linewidth=1)
    legend_profiles.append(lprofile)

    lprofile, = ax.plot(xn_me, fc*yn_me + c, \
                'r-^', label='Intermediate calibrated', alpha=a, linewidth=1)
    legend_profiles.append(lprofile)

    if plot_snr:
        plt.plot(pouter['radii_scale'], pouter['ssnr'], '.-b')
        plt.plot(pinner['radii_scale'], pinner['ssnr'], '.-r')

    #plt.vlines(pinner['radii_scale'][i], np.nanmin(pouter['smean']), np.nanmax(pinner['smean']*fc))
    r_intermediate = pinner['radii_scale'][i]

    innermask = rfa.sector_mask(dat_inner.shape, (center_outer[0], center_outer[1]), (0, i), angle_range=(0, 360))
    dat_inner_copy[~innermask] = np.nan



    outermask = rfa.sector_mask(dat_outer.shape, (center_outer[0], center_outer[1]), (i, Rtot), angle_range=(0, 360))
    dat_outer_copy[~outermask] = np.nan



    suma = np.nansum(np.dstack((dat_inner_copy*fc + c, dat_outer_copy)), axis=2)


    suma[suma == 0] = np.nan

    #suma = suma / np.nansum(suma)


    #ptotal = rf.radial_profile(suma, pixscale, function='sigma_clip', rboundaries=(0, 'Rmax'), sigma_cut=3.0, center=None, symetrize=None, save_profile=None)
    #ptotal = ptotal['radial_profile']

    #plt.plot(ptotal['radii_scale'], ptotal['smean'], '-', label='O+I N.')


    hdu_out = fits.PrimaryHDU(suma.astype(np.float32))    # Create HDU file
    hdu_out.header = hdr_outer
    hdu_out.header['SCALE'] = pixscale
    hdu_out.header.comments['SCALE'] = 'arcsec/pixel'

    hdu_out.header[zone]= i[0]
    hdu_out.header.comments[zone] = 'pixel'

    hdu_out.header['RTOT']= Rtot
    hdu_out.header.comments['RTOT'] = 'pixel'

    hdu_out.verify('fix')
    hdu_out.writeto(psf_out, overwrite=True)    # Save .fits

    PSF = {'dat_psf_total' : suma, 'im_psf_total' : psf_out, 'rjunction': i[0]}

    r_aureole = PSF['rjunction']
    im_psf_aureolewing = PSF['im_psf_total']


a = 0.95
## JUNCTION INNER-(INTERMEDIATE-OUTER)
if True:
    psf_inner = (im_psf_core, 1)
    psf_outer = (im_psf_aureolewing, 0)
    psf_out = im_psf_total

    print("psf_outer",psf_outer)
    print("psf_inner",psf_inner)
    print("psf_final",psf_out)

    rmin = rmin_c
    rmax = rmax_c
    zone = 'CJ'
    w = w_c

    psf_inner_big = psf_inner[0].replace('.fits', '_big.fits')

    ## PSF-OUTER PART
    hdu_outer = fits.open(psf_outer[0])    # HDU list of the outer part of the psf
    dat_outer = hdu_outer[psf_outer[1]].data    # Get data of the outer part of the psf
    hdr_outer = hdu_outer[psf_outer[1]].header

    #center_outer = rf.center_position(dat_outer, precission='subpixel')    # Center of the outer psf-array-dat
    shape_outer = np.shape(dat_outer)    # Shape of the outer psf-array-dat
    center_outer = rfa.center_position(shape_outer, precission='subpixel', dtype='int')


    dat_outer_copy = dat_outer.copy()    # Copy of the psf-outer array dat

    ## PSF-INNER PART
    hdu_inner = fits.open(psf_inner[0])    # HDU list of the inner part of the psf
    dat_inner = hdu_inner[psf_inner[1]].data    # Get data of the inner part of the psf
    print("dat_inener",dat_inner)
    #center_inner = rf.center_position(dat_inner.shape, precission='subpixel')    # Center of the inner psf-array-dat
    shape_inner = np.shape(dat_inner)    # Shape of the inner psf-array-dat
    center_inner = rfa.center_position(shape_inner, precission='subpixel', dtype='int')
    ###############################
    ###############################

    ## Parameters to built the star stamp with Swarp
    #PySwarpDic = {' -IMAGEOUT_NAME ' :  psf_inner_big,
    #                ' -CENTER_TYPE '   :  'MANUAL',
    #                ' -SUBTRACT_BACK ' :  'N',
    #                ' -CENTER '        :  str(center_inner[0]-1)+','+str(center_inner[1]-1),
    #                ' -IMAGE_SIZE '    :  str(shape_outer[0])+','+str(shape_outer[1]),
    #                ' -RESAMPLE '      :  'N'}

    if not os.path.isfile(psf_inner_big):
        #PySwarpOut = rfa.PySwarp((psf_inner[0], 0), configfile='default.swarp', params=PySwarpDic)
        print("LINE", center_inner)
        crop_line = ("astcrop " + psf_inner[0] +  " --hdu=1" +
                     " --mode=img --config=" + crop_cfgfile +
                     " --center=" + str(center_inner[0]+1) + "," + str(center_inner[1]+1) +
                     " --width=" + str(shape_outer[0]) + "," + str(shape_outer[1]) +
                     " --output=" + psf_inner_big)

        print(crop_line)
        os.system(crop_line)
    ###############################
    ###############################

    ## PSF-INNER PART (BIGGER)
    hdu_inner = fits.open(psf_inner_big)
    dat_inner = hdu_inner[1].data
    #dat_inner[dat_inner == 0] = np.nan
    dat_inner_copy = dat_inner.copy()

    psf_inner_big_profile = psf_inner_big.replace('.fits', '_profile.fits')
    if os.path.isfile(psf_inner_big_profile) == False:
        pinner = rfa.radial_profile(dat_inner, pixscale, function='sigma_clip', rboundaries=(0, 'Rmax'), sigma_cut=3.0, center=None, symetrize=None, save_profile=psf_inner_big_profile)

    pinner = fits.getdata(psf_inner_big_profile)


    psf_outer_big_profile = psf_outer[0].replace('.fits', '_profile.fits')
    if os.path.isfile(psf_outer_big_profile) == False:
        pouter = rfa.radial_profile(dat_outer, pixscale, function='sigma_clip', rboundaries=(0, 'Rmax'), sigma_cut=3.0, center=None, symetrize=None, save_profile=psf_outer_big_profile)

    pouter = fits.getdata(psf_outer_big_profile)



#    ## Calculate the radial profiles
#    pinner = rfa.radial_profile(dat_inner, pixscale, function='sigma_clip', rboundaries=(0, 'Rmax'), sigma_cut=3.0, center=None, symetrize=None, save_profile=None)
#    pinner = pinner['radial_profile']
#
#    pouter = rfa.radial_profile(dat_outer, pixscale, function='sigma_clip', rboundaries=(0, 'Rmax'), sigma_cut=3.0, center=None, symetrize=None, save_profile=None)
#    pouter = pouter['radial_profile']
#
#    print(pinner)
#
    ### Position in pixels of the radial profiles
    rmin_pix = np.max(np.where(pinner['radii_scale'] < rmin)[0])
    rmax_pix = np.min(np.where(pouter['radii_scale'] > rmax)[0])


    ## PROFILE SLOPES ('g=gradient')
    ginner = np.gradient(pinner['smean'])
    gouter = np.gradient(pouter['smean'])

    ## Determine which has the minimun range
    linner = len(ginner)
    louter = len(gouter)
    lmin = np.min((linner, louter))

    ## Determine the sign of the gradient
    sign_gradient = ginner[:lmin] * gouter[:lmin]

    ## Determine the difference between the slopes of the PSF and the STAR
    dif_gradient = np.abs(ginner[:lmin] - gouter[:lmin])
    dif_gradient[sign_gradient < 0.0] = np.nan    # Put nans where the difference is so big (different signs)


    ## PROFILES SIGNAL TO NOISE
    dif_sn = np.abs(pinner['ssnr'][:linner] - pouter['ssnr'][:linner])

    ## Calculate the multiplication of this TWO profiles
    ## Explanation: where dif is close to zero, the profiles will be similar in SN and Slope
    dif = dif_sn[:lmin] * dif_gradient[:lmin]    # <<-- Give more importance in difference of SN

    ## Where is the minimun of dif? --> i=radii in pixels where to normalize
    i = np.where(dif[rmin_pix:rmax_pix] == np.nanmin(dif[rmin_pix:rmax_pix]))[0] + rmin_pix



    ##################################################
    ##################################################

    c1 = pinner['smean'][i-w]
    c2 = pinner['smean'][i+w]

    w1 = pouter['smean'][i-w]
    w2 = pouter['smean'][i+w]

    fc = (w2-w1) / (c2-c1)
    c = w1 - c1 * fc
    ##################################################
    ##################################################

    """ NOT PLOTTING BECAUSE IT CAUSES AN ERROR """
    # Select log-spaced points
    rrx = pinner['radii_scale']
    rry = pinner['smean']

    # Interpolate to have few points
    xi_me = rrx
    yi_me = rry

    xi_me_min = 0.01
    xi_me_max = np.nanmax(xi_me[:-3])
    xn_me = np.geomspace(xi_me_min, xi_me_max, 26)
    yn_me = np.interp(xn_me, xi_me, yi_me)

    lprofile, = ax.plot(xn_me, yn_me, \
                         'g--<', label='Inner', alpha=a, linewidth=1)
    legend_profiles.append(lprofile)

    lprofile, = ax.plot(xn_me, fc*yn_me + c, \
                         'g->', label='Inner calibrated', alpha=a, linewidth=1)
    legend_profiles.append(lprofile)
    if plot_snr:
        plt.plot(pinner['radii_scale'], pinner['ssnr'], '.-g')

    r_core = pinner['radii_scale'][i]
    #plt.vlines(pinner['radii_scale'][i], np.nanmin(pouter['smean']), np.nanmax(pinner['smean']*fc))

    innermask = rfa.sector_mask(dat_inner.shape, (center_outer[0], center_outer[1]), (0, i), angle_range=(0, 360))
    dat_inner_copy[~innermask] = np.nan

    outermask = rfa.sector_mask(dat_outer.shape, (center_outer[0], center_outer[1]), (i, Rtot), angle_range=(0, 360))
    dat_outer_copy[~outermask] = np.nan

    suma = np.nansum(np.dstack((dat_inner_copy*fc + c, dat_outer_copy)), axis=2)

    suma[suma == 0] = np.nan

    suma = suma / np.nansum(suma)
    psf_total_profile = psf_out.replace('.fits', '_profile.fits')

    if os.path.isfile(psf_total_profile) == False:
        ptotal = rfa.radial_profile(suma, pixscale, function='sigma_clip', \
                                    rboundaries=(0, 'Rmax'), sigma_cut=3.0, \
                                    center=None, symetrize=None, save_profile=psf_total_profile)
        ptotal = ptotal['radial_profile']

    ptotal = fits.getdata(psf_total_profile)

    #plt.plot(ptotal['radii_scale'], ptotal['smean'], 'g-', label='C+O+I N.')


    hdu_out = fits.PrimaryHDU(suma.astype(np.float32))    # Create HDU file
    hdu_out.header = hdr_outer
    hdu_out.header['SCALE'] = pixscale
    hdu_out.header.comments['SCALE'] = 'arcsec/pixel'

    hdu_out.header[zone]= i[0]
    hdu_out.header.comments[zone] = 'pixel'

    hdu_out.header['RTOT']= Rtot
    hdu_out.header.comments['RTOT'] = 'pixel'

    hdu_out.verify('fix')
    hdu_out.writeto(psf_im_out, overwrite=True)    # Save .fits
    print(" ")
    print(" +++++++++++++ ")
    print(psf_im_out, "SAVED!")

    PSF = {'dat_psf_total' : suma, 'im_psf_total' : psf_out, 'rjunction': i[0]}

    r_aureole = PSF['rjunction']
    im_psf_aureolewing = PSF['im_psf_total']


a_shadow = 0.4
lregion = plt.axvspan(r_intermediate,   Rtot*0.396,     facecolor='b', \
                      alpha=a_shadow, label='Outer part')
legend_regions.append(lregion)

lregion = plt.axvspan(r_core,           r_intermediate, facecolor='r', \
                      alpha=a_shadow, label='Intermediate part')
legend_regions.append(lregion)

lregion = plt.axvspan(0,                r_core,         facecolor='g', \
                      alpha=a_shadow, label='Inner part')
legend_regions.append(lregion)


if plot_legends == True:
    first_legend = plt.legend(handles=legend_regions, loc=3, fontsize=12)
    ax1 = plt.gca().add_artist(first_legend)
    plt.legend(handles=legend_profiles, loc=1, fontsize=12)



plt.xlim(0.1, Rtot*pixscale)
if yax_min != 'min' or yax_max != 'max':
    plt.ylim(yax_min, yax_max)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('Relative flux', fontsize=16)
plt.xlabel('Distance from centre [arcsec]', fontsize=16)

axticks = [0.1, 1, 10, 100, 480]
axticklabels = [0.1, 1, 10, 100, 480]

ax.set_xticks(axticks)
ax.set_xticklabels(axticklabels)

#plt.grid()
plt.savefig(im_psf_finalplot, format='PDF', bbox_inches='tight')
print("PLOTTED", im_psf_finalplot)
