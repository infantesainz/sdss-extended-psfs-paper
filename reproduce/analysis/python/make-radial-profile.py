# Make the radial profiles of all PSFs
#
# This Python script compute the radial profile of an arbitrary image, it
# needs the image name and the extension where the data is allocated. As
# output, it will generate a .fits table with the radial profile that will
# be saved in the file specified as the last argument.
#
# Original author:
#     Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s):
# Copyright (C) 2019, Raul Infante-Sainz.
#
# This Python script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Python script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.




# Import modules
import sys
import numpy as np
from astropy.io import fits
import raulisfpy.rfastro as rfa
import raulisfpy.rfgeneral as rfg

# Read input arguments
input_image     = sys.argv[1]
input_ext       = sys.argv[2]
output_table    = sys.argv[3]
pixscale        = 0.396       # SDSS pixel scale

# Read input array image
input_data = fits.getdata(input_image, int(input_ext))

# Compute the radial profile using all image and the default center
profile = rfa.radial_profile(input_data, pixscale, function='sigma_clip', \
                            rboundaries=(0, 'Rmax'), sigma_cut=3.0, center=None, \
                            symetrize=None, save_profile=output_table)

