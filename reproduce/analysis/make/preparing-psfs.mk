# Interpolate, rotate, crop and make radial profile of all PSFs
#
# This Makefile is written to do the interpolation of the bad rows
# perpendicular to the drift scanning direction. After that the PSFs are
# rotated 90 degrees counter-clock wise in order to align the drift scanning
# direction with the left to right direction. In practical terms this
# rotation means that the sky is passing from the left to the right in the
# rotated PSFs. All PSFs are cropped to have a size of 2525 pixels
# = 16 arcmin of width (R = 8 arcmin of radius). Finally, radial profiles of
# all PSFs are computed and saved
#
# Original author:
#     Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s):
# Copyright (C) 2019, Raul Infante-Sainz.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# Directories
# -----------
#
prepared-outdir = $(BDIR)/prepared-psfs




# Joninig PSF parts
# -----------------
#
# The joining of the different parts of the PSF is done by using a Python
# script. The outer part is considered the reference and it is fixed. Then,
# the inter part is calibrated to have the same value (using radial
# profiles) at a given two radius. Finally, the outer (now outer+inter) is
# joined with the inner part, using the same technique but within a more
# internal radius. The exact radius at which they are calibrated take into
# account the signal to noise radial profile with the aim of not having a
# discontinuity because of the difference in signal to noise ratio at this
# point.
pyscript-interpolate = $(pythondir)/interpolate-bad-rows.py
interpolated-psfs = $(foreach b,$(BANDS),$(prepared-outdir)/psf-filter-$(b)-interpolated.fits)
$(prepared-outdir): ; mkdir $@
$(interpolated-psfs): $(prepared-outdir)/psf-%-interpolated.fits: \
                      $(pyscript-interpolate) \
                      $(pyconfigdir)/interpolate-rotate-%.yaml \
                      $(joined-outdir)/psf-%.fits \
                      | $(prepared-outdir)
	python $^ $@





# Rotate PSFs
# -----------
#
# The rotation of the PSFS is done in order to align them with the drift
# scanning direction. In this way, the drift scanning direction in from left
# to right. That is the traditional convention when showing astronomical
# images, the EAST pointing to the left, and NORTH to up.
rotated-psfs = $(foreach b,$(BANDS),$(prepared-outdir)/psf-filter-$(b)-rotated.fits)
$(rotated-psfs): $(prepared-outdir)/psf-%-rotated.fits: \
                 $(prepared-outdir)/psf-%-interpolated.fits
	astwarp $< --hdu=0 --rotate=90.0 --type=float32 --output=$@





# Crop PSFs to only have the defined size
# ---------------------------------------
#
# Cropping the PSFs to a particular size will make them more light. Outer
# regions will be removed. The width is 2425 pixels which correspond to 8
# arcmin in radius. This is the larger radius to be considered good for
# having high signal to noise ratio. Regions further away than this radius
# have too high noise.
cropped-psfs = $(foreach b,$(BANDS),$(prepared-outdir)/psf-filter-$(b)-cropped.fits)
$(cropped-psfs): $(prepared-outdir)/psf-%-cropped.fits: \
                 $(prepared-outdir)/psf-%-rotated.fits
	astcrop $< --hdu=1 --mode=img --config=$(astcrop-cfgfile) \
	           --center=1501,1501 --width=2425 \
		   --type=float32 --output=$@





# Generate radial profiles for each PSFs
# -------------------------------------
#
# Radial profiles are generated for each PSF. Each radial profile is saved
# in the same directory. To do the radial profiles, a Python script is used.
radial-profile-outdir = $(BDIR)/final-psf-profiles
pyscript-radial-profile = $(pythondir)/make-radial-profile.py
radial-profile-psfs = $(foreach b,$(BANDS),$(radial-profile-outdir)/psf-filter-$(b).fits)
$(radial-profile-outdir): ; mkdir $@
$(radial-profile-psfs): $(radial-profile-outdir)/psf-filter-%.fits: \
	                $(prepared-outdir)/psf-filter-%-cropped.fits \
			| $(radial-profile-outdir)

	# Compute the radial profile
	python $(pyscript-radial-profile) $< 1 $@

        # Get the current version of the PSF
	version=$$(git describe --tags)

        # Write relevat information into the radial profile table
	astfits $@ -h1 \
          --write=/,"Relevant information about this radial profile"   \
          --write=BAND,$*,"SDSS filter/band (u g r i z)"      \
          --write=SCALE,0.396,"SDSS images pixel scale (arcsec/pixel)" \
          --write=EMAIL1,"infantesainz(at)gmail(dot)com","Contact email" \
          --write=EMAIL2,"rinfante(at)iace(dot)es","Contact email" \
          --write=VERSION,$$version,"PSF version number"







# Final TeX macro
# ---------------
#
# Make an empty .tex file as final file when all PSFs have been bad rows
# interpolated, rotated and cropped.
$(mtexdir)/preparing-psfs.tex: $(cropped-psfs) $(radial-profile-psfs) | $(mtexdir)
	echo "%empty" > $@

