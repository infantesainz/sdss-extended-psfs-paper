# IMPORTANT: file can be RE-WRITTEN after './project configure'
#
# This file was created during configuration
# ('./project configure'). Therefore, it is not under
# version control and any manual changes to it will be
# over-written if the project re-configured.
#
# Local project configuration.
#
# This is just a template for the `./project configure' script to fill
# in. Please don't make any change to this file.
#
# Copyright (C) 2018-2019 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# Copying and distribution of this file, with or without modification, are
# permitted in any medium without royalty provided the copyright notice and
# this notice are preserved.  This file is offered as-is, without any
# warranty.
BDIR             = /Users/raulinfantesainz/build/sdss-extended-psfs-build
INDIR            = /Users/raulinfantesainz/input-data/sdss-extended-psfs/inputs
DEPENDENCIES-DIR = /Users/raulinfantesainz/input-data/sdss-extended-psfs/tarballs
DOWNLOADER       = /usr/local/bin/wget --no-use-server-timestamps -O
GROUP-NAME       = 
